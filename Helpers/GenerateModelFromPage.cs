using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Ineos.Models.Blocks;
using Ineos.Models.Blocks.InchMagazine;
using Ineos.Models.Pages;
using Ineos.Models.Pages.InchMagazine;
using Ineos.Models.ViewModels.InchMagazine;
using log4net;

namespace Ineos.Helpers
{
    /// <summary>
    /// Conversion Class, 1-to-1 conversion
    /// </summary>
    public static class GenerateModelFromPage
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(GenerateModelFromPage));

        #region Public Methods
        /// <summary>
        /// Generate Single Video Model from Block
        /// </summary>
        /// <param name="videoBlock"></param>
        /// <returns></returns>
        public static VideoModel GenerateVideoModel(VideoBlock videoBlock)
        {
            if (videoBlock == null)
                return new VideoModel();

            var videoModel = new VideoModel
            {
                VideoImageUrl = videoBlock.VideoWatermarkImage,
                VideoUrl = videoBlock.VideoUrl,
                VideoDescription = videoBlock.VideoBlockText
            };

            return videoModel;
        }

        /// <summary>
        /// Generate list of Video Models from List of Blocks
        /// </summary>
        /// <param name="videoBlocks"></param>
        /// <returns></returns>
        public static List<VideoModel> GenerateVideoModels(List<VideoBlock> videoBlocks)
        {
            var videos = new List<VideoModel>();

            if (videoBlocks == null)
                return videos;

            videos.AddRange(videoBlocks.Select(GenerateVideoModel));

            return videos;
        }

        /// <summary>
        /// Generate Single Article Model from article page
        /// </summary>
        /// <param name="articlePage"></param>
        /// <returns></returns>
        public static ArticleModel GenerateArticleModel(ArticlePage articlePage)
        {
            var article = new ArticleModel();

            var issuePage = EPiServerHelper.GetSpecificPageContent<IssuePage>(articlePage.ParentLink);

            article.ArticleSize = articlePage.ArticleSize;
            article.Description = articlePage.HeaderDescription;
            article.Category = GetArticleCategory(articlePage.Category);
            article.FeaturedText = articlePage.FeaturedText;
            article.IsFeatured = articlePage.IsFeatured;
            article.HeaderImage = articlePage.PageImage != null ? (articlePage.PageImage) : (EPiServerHelper.GetGenericImage());
            article.IsIntroduction = articlePage.IsIntroductionArticle;
            article.IssueDocument = issuePage.IssueUploadPdf;
            article.Tags = GetArticleCetegoryList(articlePage.Category);
            article.Title = articlePage.HeaderText;
            article.PageReference = articlePage.ContentLink;
            article.ReadTime = articlePage.ReadingTime;
            article.IssueNumber = issuePage.IssueNumber;
            article = PopulateArticleContentTextAndVideos(articlePage, article);

            return article;
        }

        /// <summary>
        /// Get the previous/next articles, these are intended for the articles page
        /// </summary>
        /// <param name="currentPage">Current page link</param>
        /// <param name="parentLink">The page's parent link</param>
        /// <returns></returns>
        public static List<KeyValuePair<ContentReference, string>> GetPreviousNextArticleLink(ContentReference currentPage, ContentReference parentLink)
        {
            var contentReferences = new List<ContentReference>();

            var children = EPiServerHelper.GetRecursiveChildren<ArticlePage>(parentLink);

            var articles = SortMoreFromInchArticles(children);

            ContentReference previousReference = null;
            ContentReference nextReference = null;

            int currentPageIndex = 0;

            for (int i = 0; i < articles.Count; i++)
            {
                if (articles[i].PageReference.ID == currentPage.ID)
                    currentPageIndex = i;
            }

            if (articles.Count == 0)
                return null;

            ContentReference firstReference = articles[0].PageReference;
            ContentReference lastReference = articles[articles.Count - 1].PageReference;

            if (articles.Count > 0 && currentPageIndex > 0)
                previousReference = articles[currentPageIndex - 1].PageReference;

            if (articles.Count > 1 && currentPageIndex < articles.Count - 1)
                nextReference = articles[currentPageIndex + 1].PageReference;

            if (previousReference != null && nextReference != null)
            {
                contentReferences.Add(previousReference);
                contentReferences.Add(nextReference);
            }
            else if (previousReference != null)
            {
                contentReferences.Add(previousReference);
                contentReferences.Add(firstReference);
            }
            else if (nextReference != null)
            {
                contentReferences.Add(lastReference);
                contentReferences.Add(nextReference);
            }
            else
            {
                contentReferences.Add(firstReference);
                contentReferences.Add(lastReference);
            }

            return (from contentReference in contentReferences
                    let page = (ArticlePage)(EPiServerHelper.GetContent(contentReference))
                    select new KeyValuePair<ContentReference, string>(contentReference, page.HeaderText)).ToList();
        }

        /// <summary>
        /// Generate list of article Models from List of article pages
        /// </summary>
        /// <param name="articlePages"></param>
        /// <returns></returns>
        public static List<ArticleModel> GenerateArticleModels(List<ArticlePage> articlePages)
        {
            var articleModels = new List<ArticleModel>();

            if (articlePages == null) return articleModels;

            articleModels.AddRange(articlePages.Select(GenerateArticleModel));

            return articleModels;
        }

        /// <summary>
        /// Generate Single issue model from issue page
        /// </summary>
        /// <param name="issuePage"></param>
        /// <returns></returns>
        public static IssueModel GenerateIssueModel(IssuePage issuePage)
        {
            var model = new IssueModel
            {
                IssueImage = issuePage.PageImage,
                IssueNumber = issuePage.IssueNumber,
                IssueUrl = issuePage.ContentLink,
                TopTitles = GetIssueTopArticles(issuePage, 4),
                IssueDocument = issuePage.IssueUploadPdf
            };

            return model;
        }

        /// <summary>
        /// Get Issues Model
        /// </summary>
        /// <param name="issuePages"></param>
        /// <returns></returns>
        public static List<IssueModel> GenerateIssueModels(List<IssuePage> issuePages)
        {
            var issueModels = new List<IssueModel>();

            if (issuePages != null && issuePages.Count > 0)
            {
                issueModels.AddRange(issuePages.Select(GenerateIssueModel));
            }

            return issueModels.OrderByDescending(x => x.IssueImage).ToList();
        } 
        #endregion

        #region Sort Methods
        public static List<RelatedArticleModel> SortInTheHeadline(IEnumerable<ArticlePage> children)
        {
            IEnumerable<ArticlePage> childrenPages = children
                    .Where(x => x.IsHeadline)
                    .OrderByDescending(x => x.Created).Take(6);

            return GetRelatedArticleModelFromPage(childrenPages);
        }

        public static List<RelatedArticleModel> SortMoreFromInchArticles(IEnumerable<ArticlePage> articles, int take = 0)
        {
            IEnumerable<ArticlePage> modifiedPages;

            if (take == 0)
            {
                modifiedPages = articles
                    .OrderBy(x => x.Created);
            }
            else
            {
                modifiedPages = articles
                    .Where(x => !x.IsIntroductionArticle)
                    .OrderBy(x => x.Created)
                    .Take(take);
            }

            return GetRelatedArticleModelFromPage(modifiedPages);
        }

        public static List<RelatedArticleModel> SortInTheIssueArticles(IEnumerable<ArticlePage> children, int take)
        {
            IEnumerable<ArticlePage> articles = children
                .Where(x => x.IssueFeatured && !x.IsFeatured)
                .OrderByDescending(x => x.IsIntroductionArticle)
                .ThenBy(x => x.Created)
                .Take(take);

            return GetRelatedArticleModelFromPage(articles);
        }
        #endregion

        #region Private Methods
        private static List<string> GetIssueTopArticles(SitePageData pageReference, int take)
        {
            var children = EPiServerHelper.GetRecursiveChildren<ArticlePage>(pageReference.PageLink);
            IEnumerable<RelatedArticleModel> articles = SortInTheIssueArticles(children, take);
            return articles.Select(articlePage => articlePage.HeaderText).ToList();
        }

        private static string PopulateArticleContentText(ArticlePage page)
        {
            string articleContentText = string.Empty;

            try
            {
                if (page.ArticleContentContentArea != null)
                {
                    var articleContentBlocks =
                        page.ArticleContentContentArea.Items.Select(
                            areaItem =>
                                EPiServerHelper.GetBlockById<ArticleSectionContentTextBlock>(areaItem.ContentLink.ID))
                            .Where(block => block != null)
                            .ToList();

                    ArticleSectionContentTextBlock contentAreaItem = articleContentBlocks.FirstOrDefault();

                    if (contentAreaItem != null)
                    {
                        articleContentText =
                            HtmlHelpers.RemoveHtmlTags(contentAreaItem.RichText.ToEditString());
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            return articleContentText;
        }

        private static ArticleModel PopulateArticleContentTextAndVideos(ArticlePage articlePage, ArticleModel article)
        {
            try
            {
                if (articlePage.ArticleContentContentArea != null)
                {
                    var articleContentBlocks =
                        articlePage.ArticleContentContentArea.Items.Select(
                            areaItem =>
                                EPiServerHelper.GetBlockById<ArticleSectionContentTextBlock>(areaItem.ContentLink.ID))
                            .Where(block => block != null)
                            .ToList();

                    ArticleSectionContentTextBlock contentAreaItem = articleContentBlocks.FirstOrDefault();

                    if (contentAreaItem != null)
                    {
                        article.ArticleContentText =
                            HtmlHelpers.RemoveHtmlTags(contentAreaItem.RichText.ToEditString());

                        var content = contentAreaItem as IContent;

                        if (content != null)
                        {
                            List<VideoBlock> videoBlocks =
                                EPiServerHelper
                                    .GetRichTextContentFragments<VideoBlock>((content).ContentLink);

                            article.Videos = GenerateVideoModels(videoBlocks);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            return article;
        }

        private static string GetArticleCategory(CategoryList categoryList)
        {
            try
            {
                if (categoryList != null && categoryList.Count > 0)
                {
                    string categoryStringArray = "[";

                    categoryStringArray += string.Format("\"{0}\"", Global.Constants.ArticleCategoryAll);

                    foreach (var cat in categoryList)
                    {
                        Category category = Category.Find(cat);
                        categoryStringArray += string.Format(",\"{0}\"", category.Name);
                    }

                    categoryStringArray += "]";

                    return categoryStringArray;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            return string.Format("[\"{0}\"]", Global.Constants.ArticleCategoryAll);
        }

        private static List<string> GetArticleCetegoryList(CategoryList categoryList)
        {
            var categories = new List<string> { Global.Constants.ArticleCategoryAll };

            try
            {
                if (categoryList != null && categoryList.Count > 0)
                {
                    foreach (var cat in categoryList)
                    {
                        Category c = Category.Find(cat);
                        categories.Add(c.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            return categories;
        }

        private static List<RelatedArticleModel> GetRelatedArticleModelFromPage(IEnumerable<ArticlePage> articles)
        {
            return articles.Select(page => new RelatedArticleModel
            {
                ArticleContentText = PopulateArticleContentText(page),
                HeaderImage = page.PageImage ?? EPiServerHelper.GetGenericImage(),
                PageReference = page.PageLink,
                ReadTime = page.ReadingTime,
                HeaderText = page.HeaderText,
                HeaderDescription = page.HeaderDescription
            }).ToList();
        }
        #endregion
    }
}