﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Framework.Localization;
using Ineos.Business.CustomTypes;
using Ineos.Models.Pages;
using Ineos.Models.Pages.InchMagazine;
using Ineos.Models.ViewModels.InchMagazine;
using log4net;

namespace Ineos.Helpers
{
    public static class MagazineHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (MagazineHelper));

        /// <summary>
        /// get issues
        /// </summary>
        /// <param name="pageReference"></param>
        /// <returns></returns>
        public static IssuesModel GetIssues(SitePageData pageReference)
        {
            var issuesModel = new IssuesModel(pageReference);

            try
            {
                List<IssuePage> result =
                    EPiServerHelper.GetRecursiveChildren<IssuePage>(pageReference.ContentLink).ToList();

                issuesModel.Issues = GenerateModelFromPage.GenerateIssueModels(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            return issuesModel;
        }

        /// <summary>
        /// This Methos uses the model to generate a CSS class for each tile in the
        /// front end, this methos is only a helper method.
        /// </summary>
        /// <param name="articleModel"></param>
        /// <returns></returns>
        public static string GenerateArticleTileCssClass(ArticleModel articleModel)
        {
            string tileSize = string.Empty;

            string tileType = string.Empty;

            string tileCssClass = string.Empty;

            try
            {
                switch (articleModel.ArticleSize)
                {
                    case ArticleSize.Small:
                        tileSize = " card-small";
                        break;
                    case ArticleSize.Medium:
                        tileSize = " card-big card-half";
                        break;
                    case ArticleSize.Large:
                        tileSize = " card-big";
                        break;
                }

                // introduction articles are always medium
                if (articleModel.IsIntroduction)
                {
                    tileType = " card-intro";
                    tileSize = " card-medium";
                }
                    // featured articles are always large
                else if (articleModel.IsFeatured)
                {
                    tileType = " featured";
                    tileSize = " card-big";
                }

                tileCssClass = "card " + tileSize + tileType;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            return tileCssClass;
        }

        /// <summary>
        /// Get All Articles Model to Display on the Articles results page
        /// </summary>
        /// <param name="pageReference"></param>
        /// <returns></returns>
        public static ArticlesModel GetAllArticlesModel(SitePageData pageReference)
        {
            var articlesModel = new ArticlesModel(pageReference);

            try
            {
                var pages = EPiServerHelper.GetRecursiveChildren<ArticlePage>(pageReference.ContentLink)
                    .Where(x=>!x.IsHeadline)
                    .ToList();

                if (!pages.Any())
                {
                    return new ArticlesModel(pageReference);
                }

                List<ArticlePage> children =
                    EPiServerHelper.GetRecursiveChildren<ArticlePage>(pageReference.PageLink).ToList();

                List<ArticleModel> articleModel = GenerateModelFromPage.GenerateArticleModels(pages);

                articlesModel = new ArticlesModel(pageReference)
                {
                    InTheHeadline = GenerateModelFromPage.SortInTheHeadline(children),
                    MoreFromInchArticles = GenerateModelFromPage.SortMoreFromInchArticles(children),
                    InTheIssueArticles = GenerateModelFromPage.SortInTheIssueArticles(children, 3),
                    Articles = articleModel.OrderByDescending(x => x.IssueNumber)
                        .ThenByDescending(x => x.IsIntroduction)
                        .ThenByDescending(x => x.IsFeatured)
                        .ThenByDescending(x => x.CreatedDateTime)
                        .ToList()
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            return articlesModel;
        }

        /// <summary>
        /// Get Articles Model but ONLY contains latest articles!!.
        /// </summary>
        /// <param name="pageReference"></param>
        /// <returns></returns>
        public static ArticlesModel GetArticleModel(SitePageData pageReference)
        {
            var articlesModel = new ArticlesModel(pageReference);

            List<KeyValuePair<ContentReference, string>> previousNextArticles =
                GenerateModelFromPage.GetPreviousNextArticleLink(pageReference.ContentLink, pageReference.ParentLink);

            List<ArticlePage> children =
                EPiServerHelper.GetRecursiveChildren<ArticlePage>(pageReference.ParentLink).ToList();

            try
            {
                articlesModel = new ArticlesModel(pageReference)
                {
                    //RelatedArticleModels = GenerateModelFromPage.SortMoreFromInchArticles(pageReference.ParentLink),
                    InTheHeadline = GenerateModelFromPage.SortInTheHeadline(children),
                    MoreFromInchArticles = GenerateModelFromPage.SortMoreFromInchArticles(children),
                    InTheIssueArticles = GenerateModelFromPage.SortInTheIssueArticles(children, 3),
                    Articles = new List<ArticleModel>(),
                    PreviousPage = previousNextArticles[0],
                    NextPage = previousNextArticles[1]
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            return articlesModel;
        }

        /// <summary>
        /// Get article page link
        /// </summary>
        /// <returns></returns>
        public static ContentReference ArticlePageLink()
        {
            var startPage = EPiServerHelper.GetContentOfType<InchStartPage>(EPiServerHelper.GetInchStartPage());
            if (startPage == null) return null; 
            return startPage.ArticlePageLink;
        }

        /// <summary>
        /// get issue page link
        /// </summary>
        /// <returns></returns>
        public static ContentReference IssuePageLink()
        {
            var startPage = EPiServerHelper.GetContentOfType<InchStartPage>(EPiServerHelper.GetInchStartPage());
            if (startPage == null) return null; 
            
            return startPage.IssuePageLink;
        }

        /// <summary>
        /// Get Footer Model from homepage
        /// </summary>
        /// <returns></returns>
        public static ContactModel GetFooterContactDetails()
        {
            var startPage = EPiServerHelper.GetContentOfType<InchStartPage>(EPiServerHelper.GetInchStartPage());

            if (startPage == null) return null;

            var model = new ContactModel
            {
                FooterHeading = startPage.FooterHeading,
                CopyrightText = startPage.CopyrightText,
                EditorName = startPage.EditorName,
                EditorialAddress = startPage.EditorialAddress,
                Email = startPage.Email,
                Photography = startPage.Photography,
                Publisher = startPage.Publisher,
                SpreadTheWordTextContent = startPage.SpreadTheWordTextContent,
                LogoLink = startPage.FooterLogo,
                SecondaryLogo = startPage.SecondaryLogo
            };

            return model;
        }

        public static string Truncate(this string value, int maxChars)
        {
            if (value == null) return string.Empty;
            return value.Length <= maxChars ? value : value.Substring(0, maxChars) + " ..";
        }

        /// <summary>
        /// Get a list of categories to display on
        /// the issue dropdown
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetArticleCategories()
        {
            var rootCategory = Category.Find(Global.Constants.ArticleCategoryRoot);

            var cats = new Dictionary<int, string> { { 0, Global.Constants.ArticleCategoryAll } };

            if (rootCategory != null && (rootCategory.Categories != null && rootCategory.Categories.Count > 0))
            {
                CategoryCollection categories = rootCategory.Categories;

                foreach (Category category in categories)
                {
                    string localizedCategory = LocalizationService.Current.GetString(string.Format("/Categories/{0}", category.Name));

                    cats.Add(category.ID, localizedCategory);
                }
            }

            return cats;
        }

        /// <summary>
        /// Get a list of enabled languages
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> GetEnabledLanguages()
        {
            try
            {
                PageDataCollection langs = DataFactory.Instance.GetLanguageBranches(ContentReference.StartPage);

                return
                    (from lang in langs let l = lang.Language.DisplayName select lang).ToDictionary(
                        lang => lang.Language.Name, lang => lang.Language.NativeName);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            return null;
        }

        /// <summary>
        /// Get Page Content of a specific culture
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="languageKey"></param>
        /// <returns></returns>
        public static IContent GetPageContent(ContentReference currentPage, string languageKey)
        {
            return EPiServerHelper.GetContent(currentPage, languageKey);
        }

        /// <summary>
        /// Get Page Content
        /// </summary>
        /// <param name="currentPage"></param>
        /// <returns></returns>
        public static IContent GetPageContent(ContentReference currentPage)
        {
            return EPiServerHelper.GetContent(currentPage);
        }

        /// <summary>
        /// Given a page URL, this method will alter the URL and preform the required checks on the page
        /// </summary>
        /// <param name="url"></param>
        /// <param name="workingLanguage"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static string ResolvePageLanguageUrl(string url, string workingLanguage, ContentReference page)
        {
            if (EPiServerHelper.GetContent(page, workingLanguage) == null)
                return string.Empty;

            string[] urlSegments = url.Split('/');

            string result = string.Empty;

            if (workingLanguage != "en")
                result = workingLanguage + "/";

            if (GetEnabledLanguages().ContainsKey(urlSegments[0]))
                result += String.Join("/", urlSegments.Where((x, index) => index != 0));
            else
                result += String.Join("/", urlSegments);

            return result;
        }
    }
}