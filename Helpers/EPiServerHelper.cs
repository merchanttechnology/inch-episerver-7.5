using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Security;
using EPiServer;
using EPiServer.Core;
using EPiServer.Core.Html.StringParsing;
using EPiServer.ServiceLocation;
using Ineos.Models.Blocks;
using Ineos.Models.Blocks.InchMagazine;
using Ineos.Models.Pages;
using Ineos.Models.Pages.InchMagazine;
using log4net;

namespace Ineos.Helpers
{
    public static class EPiServerHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(MagazineHelper));

        private static void GetChildrenRecursive(ContentReference parent, List<IContent> list, IContentRepository contentRepository)
        {
            foreach (var child in contentRepository.GetChildren<IContent>(parent))
            {
                list.Add(child);
                GetChildrenRecursive(child.ContentLink, list, contentRepository);
            }
        }

        /// <summary>
        /// Recursivly Itterate through all children and get all pages if type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reference"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetRecursiveChildren<T>(ContentReference reference)
        {
            var contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            var list = new List<IContent>();
            GetChildrenRecursive(reference, list, contentRepository);

            IEnumerable<T> pages =
                list.Where(x => x.GetOriginalType() == typeof(T)).Select(x => (T)x);

            return pages;
        }

        /// <summary>
        /// Get all children of type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reference"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetChildren<T>(ContentReference reference)
        {
            var contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            IEnumerable<IContent> children = contentRepository.GetChildren<IContent>(reference);

            IEnumerable<T> pages =
                children.Where(x => x.GetOriginalType() == typeof(T)).Select(x => (T)x);

            return pages;
        }

        /// <summary>
        /// Get All Properties in an Object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static IEnumerable<PropertyInfo> GetProperties(object obj)
        {
            return
                obj.GetType()
                    .GetProperties()
                    .Where(x => x.PropertyType == typeof(String) || x.PropertyType == typeof(XhtmlString))
                    .ToArray();
        }

        /// <summary>
        /// Get Any Content based on Reference
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        public static IContent GetContent(ContentReference reference)
        {
            IContent content = ServiceLocator.Current.GetInstance<IContentRepository>().Get<IContent>(reference);
            return content;
        }

        /// <summary>
        /// Get Language Branches
        /// </summary>
        /// <param name="reference">Any Page Reference</param>
        /// <returns></returns>
        public static List<string> GetLanguageBaranches(ContentReference reference)
        {
            var results =
                ServiceLocator.Current.GetInstance<IContentRepository>()
                    .GetLanguageBranches<IContent>(reference)
                    .Select(x => x.Property.LanguageBranch)
                    .ToList();

            return results;
        }

        /// <summary>
        /// Get Any Content based on Reference
        /// </summary>
        /// <param name="reference"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public static IContent GetContent(ContentReference reference, string language)
        {
            try
            {
                var page = GetSpecificPageContent<SitePageData>(reference, new LanguageSelector(language));
                if (page != null && page.Status == VersionStatus.Published)
                    return page;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            return null;
        }

        /// <summary>
        /// Get Specific Page Content based on Reference and Culture
        /// </summary>
        /// <param name="reference"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public static T GetSpecificPageContent<T>(ContentReference reference, LanguageSelector lang) where T : SitePageData
        {
            T content = ServiceLocator.Current.GetInstance<IContentRepository>().Get<T>(reference, lang);
            return content;
        }

        /// <summary>
        /// Get Specific Page Content based on Reference
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        public static T GetSpecificPageContent<T>(ContentReference reference) where T : SitePageData
        {
            T content = ServiceLocator.Current.GetInstance<IContentRepository>().Get<T>(reference);
            return content;
        }

        /// <summary>
        /// Get Specific Block Content based on Reference
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        public static T GetSpecificBlockContent<T>(ContentReference reference) where T : SiteBlockData
        {
            T content = ServiceLocator.Current.GetInstance<IContentRepository>().Get<T>(reference);

            return content;
        }

        /// <summary>
        /// Get block content by content id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="contentId"></param>
        /// <returns></returns>
        public static T GetBlockById<T>(int contentId) where T : SiteBlockData
        {
            T content = null;
            // Get the equivalent of DataFactory in CMS 5/6
            var repository = ServiceLocator.Current.GetInstance<IContentRepository>();

            // Get content (page or block) with ID 5
            var contentReference = new ContentReference(contentId);

            if (repository.Get<SiteBlockData>(contentReference).GetOriginalType() == typeof(T))
            {
                content = repository.Get<T>(contentReference);
            }

            return content;
        }

        /// <summary>
        /// Get Content from TinyMce Control
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reference"></param>
        /// <returns></returns>
        public static List<T> GetRichTextContentFragments<T>(ContentReference reference) where T : SiteBlockData
        {
            XhtmlString richTextProperty = null;

            var block = GetContent(reference);

            List<T> itemsList = new List<T>();

            var textBlock = block as ArticleSectionContentTextBlock;

            if (textBlock != null)
                richTextProperty = textBlock.RichText;

            if (richTextProperty != null)
            {
                StringFragmentCollection content = richTextProperty.Fragments;

                foreach (IStringFragment item in content)
                {
                    var fragment = item as ContentFragment;
                    if (fragment != null)
                    {
                        var link = fragment.ContentLink;
                        itemsList.Add(GetBlockById<T>(link.ID));
                    }
                }
            }

            return itemsList;
        }

        /// <summary>
        /// get page of a particular type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetContentOfType<T>(PageReference reference) where T : SitePageData
        {
            if (reference == null)
                return null;

            return ServiceLocator.Current.GetInstance<IContentRepository>().Get<T>(reference);
        }

        /// <summary>
        /// Get Inch Start Page
        /// </summary>
        /// <returns></returns>
        public static PageReference GetInchStartPage()
        {
            try
            {
                // if the inch homepage is the start page, then return this
                var rootPageData = GetContentOfType<SitePageData>(ContentReference.StartPage);

                InchStartPage startPage = rootPageData.GetOriginalType() == typeof (InchStartPage)
                    ? GetContentOfType<InchStartPage>(ContentReference.StartPage)
                    : GetRecursiveChildren<InchStartPage>(ContentReference.StartPage).FirstOrDefault();

                // if inch hoempage is not the start page then go through the children and return the inch magazine homepage

                if (startPage != null) return startPage.PageLink;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            return null;

        }

        /// <summary>
        /// Get Default Generic Image.
        /// </summary>
        /// <returns></returns>
        public static ContentReference GetGenericImage()
        {
            try
            {
                var startPage = GetContentOfType<InchStartPage>(GetInchStartPage());
                if (startPage != null) return startPage.PageFallbackImage;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            return null;
        }

        public static bool Login(string username, string password)
        {
            bool result = Membership.ValidateUser(username, password);

            return result;
        }
    }
}