﻿using System.Collections.Generic;
using System.Linq;
using Ineos.Database;
using Ineos.Database.Model.Tables;
using Ineos.Database.Service;
using Ineos.Models.ViewModels.Ineos;

namespace Ineos.Helpers
{
    public static class DatabaseHelper
    {
        private static readonly IProductDataService<productdata> Service =
            InitialisationModule.GetService<productdata>();

        public static List<string> GetBusinesses()
        {
            List<string> result = Service.GetAll().Select(x => x.bu).Distinct().ToList();
            return result;
        }

        public static List<MarketsListViewModel> GetMarkets()
        {
            return null;
        }
    }
}