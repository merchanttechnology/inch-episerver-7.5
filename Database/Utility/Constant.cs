﻿using System.Configuration;

namespace Ineos.Database.Utility
{
    public class Constant
    {
        public static string DatabaseConnection = ConfigurationManager.ConnectionStrings["IneosProductsConnectionString"].ConnectionString;
        public static string TestDatabaseConnection = ConfigurationManager.ConnectionStrings["TestIneosProductsConnectionString"].ConnectionString;
        //public static string DatabaseConnection = ConfigurationManager.ConnectionStrings["EPiServerDB"].ConnectionString;
        //public static string TestDatabaseConnection = ConfigurationManager.ConnectionStrings["EPiServerDB"].ConnectionString;

        public static string LastModifiedDate = "LastModified";

        /// <summary>
        /// Get last modified date/time of product database, this is a system query, and should not change
        /// unless the product databasename changes.
        /// </summary>
        public static string DatabaseLastModifiedDateTimeSql = "SELECT  top 1 last_user_update " +
                                                               "FROM    sys.dm_db_index_usage_stats " +
                                                               "WHERE   database_id = DB_ID( 'dbIneosProducts') " +
                                                               "AND     last_user_update is not null " +
                                                               "ORDER BY last_user_update desc";

        public static string FieldsdefinitionCacheKey = "initialCache_fieldsdefinition";
        public static string SqldefinitionCacheKey = "initialCache_sqldefinition";
        public static string ViewPublishedProductdataCacheKey = "initialCache_v_published_productdata";

        public static string KeywordCacheKey = "initialCache_KeywordCacheKey";
        public static string KeywordReferenceCacheKey = "initialCache_KeywordReferenceCacheKey";
        public static string TechnologyReferenceCacheKey = "initialCache_TechnologyReferenceCacheKey";
    }
}
