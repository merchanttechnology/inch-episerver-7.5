﻿using System.Web.Mvc;
using Castle.MicroKernel;
using Ineos.Database.Model;
using Ineos.Database.Model.Tables;
using Ineos.Database.Model.Views;
using Ineos.Database.Repository;
using Ineos.Database.Service;
using StructureMap;
using EPiServer.ServiceLocation;

namespace Ineos.Database
{
    public static class InitialisationModule
    {
        public static IProductDataService<T> GetService<T>() where T : BaseEntity
        {
            //ServiceLocator.Current.GetInstance<IProductDataService<T>>();

            return System.Web.Mvc.DependencyResolver.Current.GetService<IProductDataService<T>>();
        }

        //static InitialisationModule()
        //{
        //}

        //private static IKernel kernel;

        //public static void InitNinject()
        //{
        //    Initialise<fieldsdefinition>();
        //    Initialise<sqldefinition>();
        //    Initialise<v_published_productdata>();
        //    Initialise<Keyword>();
        //    Initialise<KeywordReference>();
        //    Initialise<productdata>();

        //    //Initialize businesses
        //    Initialise<barex>();
        //    Initialise<ab>();
        //    Initialise<chlorvinyl>();
        //    Initialise<enterprise>();
        //    Initialise<nitrile>();
        //    Initialise<oligomer>();
        //    Initialise<opeu>();
        //    Initialise<opu>();
        //    Initialise<oxide>();
        //    Initialise<paraform>();
        //    Initialise<phenol>();
        //    Initialise<refining>();
        //    Initialise<styrenic>();
        //    Initialise<Technology>();
        //}

        //public static IProductDataService<T> Initialise<T>() where T : BaseEntity
        //{
        //    Bind<T>();

        //    return GetService<T>();
        //}

        //private static void UpdateCache(IProductDataService<T> service)
        //{
        //    var modifyDateTime = service.GetDatabaseLastModifiedDataTime();
        //}

        //public static IProductDataService<T> GetService<T>() where T : BaseEntity
        //{
        //    //return kernel.Get<IProductDataService<T>>();
        //    //new ConfigurationExpression().ge
        //    return System.Web.Mvc.DependencyResolver.Current.GetService<IProductDataService<T>>();
        //}

        //public static void Bind<T>() where T : BaseEntity
        //{
        //    kernel.Bind<IProductDataService<T>>().To<ProductDataService<T>>();
        //    kernel.Bind<IProductRepository<T>>().To<ProductDataRepository<T>>();
        //}
    }
}