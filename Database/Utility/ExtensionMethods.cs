﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ineos.Database.Utility
{
    public static class ExtensionMethods
    {
        public static double AbsoluteDateTimeDifferenceInMinutes(DateTime dt1, DateTime dt2)
        {
            TimeSpan difference = (dt1 - dt2);
            return Math.Abs(difference.TotalMinutes);
        }

        public static double DateTimeDifferenceInMinutes(DateTime dt1, DateTime dt2)
        {
            TimeSpan difference = (dt1 - dt2);
            return difference.TotalMinutes;
        }

        public static List<Type> GetChildrenType<T>() where T : class
        {
            List<Type> typesList =
                typeof (T)
                    .Assembly
                    .GetTypes()
                    .Where(
                        type => type != typeof (T)
                                && typeof (T).IsAssignableFrom(type)
                    ).ToList();
            return typesList;
        }
    }
}
