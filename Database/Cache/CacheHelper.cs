﻿using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Web.Mvc;
using Ineos.Database.Model;
using Ineos.Database.Model.Tables;
using Ineos.Database.Model.Views;
using Ineos.Database.Service;
using Ineos.Database.Utility;

namespace Ineos.Database.Cache
{
    public static class CacheHelper
    {
        //public static IProductDataService<T> GetService<T>() where T : BaseEntity
        //{
        //    return System.Web.Mvc.DependencyResolver.Current.GetService<IProductDataService<T>>();
        //}

        public static void RefreshCache()
        {
            IProductDataService<productdata> service = InitialisationModule.GetService<productdata>();

            var dbCacheDateTime = service.GetDatabaseLastModifiedDataTime();

            if (CacheManager.DbCacheDateTime != dbCacheDateTime)
            {
                InitialiseObject<fieldsdefinition>(Constant.FieldsdefinitionCacheKey);
                InitialiseObject<sqldefinition>(Constant.SqldefinitionCacheKey);
                InitialiseObject<v_published_productdata>(Constant.ViewPublishedProductdataCacheKey);
                InitialiseObject<Technology>(Constant.TechnologyReferenceCacheKey);

                InitialiseObject<Keyword>(Constant.KeywordCacheKey);
                InitialiseObject<KeywordReference>(Constant.KeywordReferenceCacheKey);

                CacheManager.DbCacheDateTime = dbCacheDateTime;
            }
        }

        //TODO: log any errors using a logger.
        private static void InitialiseObject<T>(string key) where T : BaseEntity
        {
            var service = InitialisationModule.GetService<T>();
            List<T> results = service.GetAll().ToList();

            if (CacheManager.Exists<List<T>>(key))
                CacheManager.Remove<List<T>>(key);

            CacheManager.Add(key, results);
        }

        public static void UpdateCache(object source, ElapsedEventArgs e)
        {
            RefreshCache();
        }

        public static IEnumerable<T> GetCachedObject<T>(string key) where T : BaseEntity
        {
            IEnumerable<T> q = CacheManager.Get<List<T>>(key);
            return q.AsEnumerable();
        }
    }
}