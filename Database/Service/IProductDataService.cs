﻿using Ineos.Database.Model;

namespace Ineos.Database.Service
{
    public interface IProductDataService<T> : IEntityService<T> where T : BaseEntity
    {
    }
}
