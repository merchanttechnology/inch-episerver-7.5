using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Ineos.Database.Model;

namespace Ineos.Database.Service
{
    public interface IEntityService<T> : IService
        where T : BaseEntity
    {
        void Create(T entity);
        void Delete(T entity);
        IEnumerable<T> GetAll();      
        void Update(T entity);
        IQueryable<T> Get();
        IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate);
        DateTime GetDatabaseLastModifiedDataTime();
    }
}