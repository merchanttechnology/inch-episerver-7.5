﻿using Ineos.Database.Model;
using Ineos.Database.Repository;

namespace Ineos.Database.Service
{
    public class ProductDataService<T> : EntityService<T>, IProductDataService<T> where T : BaseEntity
    {
        readonly IProductRepository<T> _productRepository;

        public ProductDataService(IUnitOfWork unitOfWork, IProductRepository<T> productRepository)
            : base(unitOfWork, productRepository)
        {
            _productRepository = productRepository;
        }
    }
}