﻿using Ineos.Database.Model;

namespace Ineos.Database.Repository
{
    public interface IProductRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
    }
}