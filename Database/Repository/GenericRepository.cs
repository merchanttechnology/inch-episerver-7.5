﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Ineos.Database.Model;
using Ineos.Database.Utility;
using LinqKit;

namespace Ineos.Database.Repository
{
    public abstract class GenericCacheRepository<T> : IGenericRepository<T> where T : BaseEntity
    {

        public IEnumerable<T> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public T Add(T entity)
        {
            throw new NotImplementedException();
        }

        public T Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(T entity)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Get()
        {
            throw new NotImplementedException();
        }

        public DateTime GetDatabaseLastModifiedDataTime()
        {
            throw new NotImplementedException();
        }
    }

    public abstract class GenericRepository<T> : IGenericRepository<T>
        where T : BaseEntity
    {
        protected DbContext Entities;
        protected readonly IDbSet<T> Dbset;

        protected GenericRepository(DbContext context)
        {
            Entities = context;
            Dbset = context.Set<T>();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return Dbset.AsEnumerable();
        }

        public virtual IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            IEnumerable<T> query = Dbset.AsExpandable().Where(predicate).AsEnumerable();
            return query;
        }

        public virtual IQueryable<T> Get()
        {
            return Dbset.AsQueryable();
        }

        public virtual T Add(T entity)
        {
            return Dbset.Add(entity);
        }

        public virtual T Delete(T entity)
        {
            return Dbset.Remove(entity);
        }

        public virtual void Edit(T entity)
        {
            Entities.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Save()
        {
            Entities.SaveChanges();
        }

        public DateTime GetDatabaseLastModifiedDataTime()
        {
            DateTime dbLastModifiedDateTime =
                Entities.Database.SqlQuery<DateTime>(Constant.DatabaseLastModifiedDateTimeSql).FirstOrDefault();
            return dbLastModifiedDateTime;
        }

        //public bool IsCacheUpToDate()
        //{
        //    return
        //        ExtensionMethods.DateTimeDifferenceInMinutes(GetDatabaseLastModifiedDataTime(),
        //            CacheHelper.GetLastModifiedDataTime().dateTime) <= 1;
        //}

        //public virtual IEnumerable<T> FindPredicate(DbSet<T> dbSet, Expression<Func<T, bool>> predicate)
        //{
        //    var local = dbSet.Local.Where(predicate.Compile());
        //    return local.Any()
        //        ? local
        //        : dbSet.Where(predicate).ToArray();
        //}

        //public virtual async Task<IEnumerable<T>> FindPredicateAsync<T>(DbSet<T> dbSet,
        //    Expression<Func<T, bool>> predicate) where T : class
        //{
        //    var local = dbSet.Local.Where(predicate.Compile());
        //    return local.Any()
        //        ? local
        //        : await dbSet.Where(predicate).ToArrayAsync().ConfigureAwait(false);
        //}
    }
}