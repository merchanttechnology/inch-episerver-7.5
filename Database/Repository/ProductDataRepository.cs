﻿using System.Data.Entity;
using Ineos.Database.Model;

namespace Ineos.Database.Repository
{
    public class ProductDataRepository<T> : GenericRepository<T>, IProductRepository<T> where T : BaseEntity
    {
        public ProductDataRepository(DbContext context) : base(context) { }
    }

    public class ProductCacheDataRepository<T> : GenericRepository<T>, IProductRepository<T> where T : BaseEntity
    {
        public ProductCacheDataRepository(DbContext context) : base(context) { }
    }
}