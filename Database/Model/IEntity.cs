﻿namespace Ineos.Database.Model
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}