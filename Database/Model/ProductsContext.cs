﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using Ineos.Database.Model.Tables;
using Ineos.Database.Model.Views;
using Ineos.Database.Utility;
using System.Runtime.Caching;

namespace Ineos.Database.Model
{
    public class ProductsCacheContext : MemoryCache
    {
        public ProductsCacheContext() : base("memorycahce"){}
    }

    public class ProductsDBContext : DbContext
    {
        public ProductsDBContext() : base(Constant.DatabaseConnection){}

        public DbSet<ab> Ab { get; set; }
        public DbSet<barex> Barex { get; set; }
        public DbSet<productdata> Products { get; set; }
        public DbSet<fieldsdefinition> Fieldsdefinitions { get; set; }
        public DbSet<sqldefinition> Sqldefinitions { get; set; }
        public DbSet<v_published_productdata> VPublishedProductdata { get; set; }
        public DbSet<Keyword> Keyword { get; set; }
        public DbSet<KeywordReference> KeywordReferences { get; set; }
        public DbSet<Technology> TechnoloogyReferences { get; set; }

        public override int SaveChanges()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditableEntity
                            &&
                            (x.State == EntityState.Added ||
                             x.State == EntityState.Modified));

            foreach (var entry in modifiedEntries)
            {
                var entity = entry.Entity as IAuditableEntity;
                if (entity != null)
                {
                    var identityName = Thread.CurrentPrincipal.Identity.Name;
                    DateTime now = DateTime.UtcNow;

                    if (entry.State == EntityState.Added)
                    {
                        entity.CreatedBy = identityName;
                        entity.CreatedDate = now;
                    }
                    else
                    {
                        Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                        Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                    }

                    entity.UpdatedBy = identityName;
                    entity.UpdatedDate = now;
                }
            }

            return base.SaveChanges();
        }
    }
}