using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    public class oligomer : BaseEntity, ITable
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string grade { get; set; }

        public int? molecularweight { get; set; }

        [StringLength(100)]
        public string kinematicviscosity { get; set; }

        [StringLength(100)]
        public string acronym { get; set; }

        [StringLength(100)]
        public string carbonchain { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string bu { get; set; }

        [StringLength(100)]
        public string regulatorydocuments { get; set; }

        public int sortorder { get; set; }

        [Required]
        public virtual productdata productdata { get; set; }
    }
}
