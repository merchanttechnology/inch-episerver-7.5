using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    public class Technology : BaseEntity, ITable
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string grade { get; set; }

        [StringLength(100)]
        public string contact { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string bu { get; set; }

        [StringLength(100)]
        public string msds { get; set; }

        [StringLength(100)]
        public string techdatasheet { get; set; }

        [Required]
        public virtual productdata productdata { get; set; }
    }
}
