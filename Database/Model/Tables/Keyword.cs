using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    [Table("Keyword")]
    public class Keyword : BaseEntity, ITable
    {
        public Keyword()
        {
            KeywordReferences = new HashSet<KeywordReference>();
        }

        public int KeywordId { get; set; }

        [Required]
        [StringLength(255)]
        public string Term { get; set; }

        public int Count { get; set; }

        public virtual ICollection<KeywordReference> KeywordReferences { get; set; }
    }
}
