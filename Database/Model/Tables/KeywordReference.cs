using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    [Table("KeywordReference")]
    public class KeywordReference : BaseEntity, ITable
    {
        public int KeywordReferenceId { get; set; }

        public int KeywordId { get; set; }

        [Required]
        [StringLength(255)]
        public string ProductGrade { get; set; }

        [Required]
        [StringLength(255)]
        public string ProductBu { get; set; }

        public virtual Keyword Keyword { get; set; }
    }
}
