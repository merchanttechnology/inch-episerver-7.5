using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    [Table("productdata")]
    public class productdata : BaseEntity, ITable
    {
        //public productdata()
        //{
        //    attachedfiles = new HashSet<attachedfile>();
        //}

        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string grade { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string bu { get; set; }

        [StringLength(100)]
        public string country { get; set; }

        [StringLength(100)]
        public string manufacturingsite { get; set; }

        [StringLength(100)]
        public string brand { get; set; }

        [StringLength(100)]
        public string material { get; set; }

        [StringLength(100)]
        public string submaterial { get; set; }

        [StringLength(255)]
        public string industry { get; set; }

        [StringLength(255)]
        public string subindustry { get; set; }

        public string description { get; set; }

        [StringLength(100)]
        public string owner { get; set; }

        public string applications { get; set; }

        public bool online { get; set; }

        public virtual ab ab { get; set; }

        public virtual ICollection<attachedfile> attachedfiles { get; set; }

        public virtual barex barex { get; set; }

        public virtual chlorvinyl chlorvinyl { get; set; }

        public virtual enterprise enterprise { get; set; }

        public virtual nitrile nitrile { get; set; }

        public virtual oligomer oligomer { get; set; }

        public virtual opeu opeu { get; set; }

        public virtual opu opu { get; set; }

        public virtual oxide oxide { get; set; }

        public virtual paraform paraform { get; set; }

        public virtual phenol phenol { get; set; }

        public virtual refining refining { get; set; }

        public virtual styrenic styrenic { get; set; }

        public virtual Technology technology { get; set; }
    }
}
