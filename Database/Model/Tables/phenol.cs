using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    [Table("phenol")]
    public class phenol : BaseEntity, ITable
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string grade { get; set; }

        [StringLength(255)]
        public string physicalproperties { get; set; }

        [StringLength(255)]
        public string mainderivatives { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string bu { get; set; }

        [Required]
        public virtual productdata productdata { get; set; }
    }
}
