using Ineos.Database.Model;

namespace Ineos.Database.Model.Tables
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class attachedfile : BaseEntity, ITable
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string Grade { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string Bu { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(100)]
        public string Documenttype { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(5)]
        public string Documentlanguage { get; set; }

        [Required]
        [StringLength(100)]
        public string Filename { get; set; }

        [Required]
        [StringLength(100)]
        public string Mimetype { get; set; }

        [Required]
        [StringLength(32)]
        public string Checksum { get; set; }

        [Required]
        public virtual productdata Productdata { get; set; }

        public virtual Filescontent Filescontent { get; set; }
    }
}
