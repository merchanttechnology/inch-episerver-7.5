using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    [Table("barex")]
    public class barex : BaseEntity, ITable
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string bu { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string grade { get; set; }

        [StringLength(100)]
        public string mfr { get; set; }

        [StringLength(100)]
        public string density { get; set; }

        [Required]
        public virtual productdata productdata { get; set; }
    }
}
