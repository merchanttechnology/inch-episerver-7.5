using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    [Table("fieldsdefinition")]
    public class fieldsdefinition : BaseEntity, ITable, ICacheRefreshable
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string bu { get; set; }

        [Key]
        [Column(Order = 1)]
        public string field { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(100)]
        public string fieldalias { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int fieldindex { get; set; }

        public int? fieldwidth { get; set; }

        [StringLength(100)]
        public string fieldaligment { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(100)]
        public string tabletype { get; set; }
    }
}
