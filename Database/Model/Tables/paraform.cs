using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    [Table("paraform")]
    public class paraform : BaseEntity, ITable
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string grade { get; set; }

        [StringLength(100)]
        public string molecularweight { get; set; }

        [StringLength(100)]
        public string technology { get; set; }

        [StringLength(100)]
        public string availablequalities { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string bu { get; set; }

        [StringLength(255)]
        public string options { get; set; }

        [Required]
        public virtual productdata productdata { get; set; }
    }
}
