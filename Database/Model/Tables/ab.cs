using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    public class ab : BaseEntity, ITable
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string bu { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string grade { get; set; }

        [StringLength(100)]
        public string mfr { get; set; }

        [StringLength(100)]
        public string izod { get; set; }

        [StringLength(100)]
        public string tensilestress { get; set; }

        [StringLength(100)]
        public string vicat { get; set; }

        [StringLength(100)]
        public string density { get; set; }

        [Required]
        public virtual productdata productdata { get; set; }
    }
}
