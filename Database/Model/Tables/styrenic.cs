using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    public class styrenic : BaseEntity, ITable
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string grade { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string bu { get; set; }

        [StringLength(100)]
        public string prefoameddensity { get; set; }

        [StringLength(100)]
        public string beadsize { get; set; }

        [StringLength(100)]
        public string mouldeddensity { get; set; }

        [Required]
        public virtual productdata productdata { get; set; }
    }
}
