using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    public class chlorvinyl : BaseEntity, ITable
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string grade { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string bu { get; set; }

        [StringLength(255)]
        public string physicalproperties { get; set; }

        [StringLength(100)]
        public string options { get; set; }

        public int? kvalue { get; set; }

        [Required]
        public virtual productdata productdata { get; set; }
    }
}
