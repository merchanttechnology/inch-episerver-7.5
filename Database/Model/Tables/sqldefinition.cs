using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    [Table("sqldefinition")]
    public class sqldefinition : BaseEntity, ITable, ICacheRefreshable
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string bu { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string clausepart { get; set; }

        [Required]
        public string clausetext { get; set; }
    }
}
