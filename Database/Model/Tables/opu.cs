using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    public class opu : BaseEntity, ITable
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string grade { get; set; }

        [StringLength(100)]
        public string mfr { get; set; }

        public float? density { get; set; }

        [StringLength(100)]
        public string notchedizodimpact { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string bu { get; set; }

        [Required]
        public virtual productdata productdata { get; set; }
    }
}
