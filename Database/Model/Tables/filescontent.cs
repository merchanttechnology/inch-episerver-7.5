using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Tables
{
    [Table("filescontent")]
    public sealed class Filescontent : BaseEntity, ITable
    {
        public Filescontent()
        {
            Attachedfiles = new HashSet<attachedfile>();
        }

        [Key]
        [StringLength(32)]
        public string Checksum { get; set; }

        [Required]
        public string Documentcontent { get; set; }

        public ICollection<attachedfile> Attachedfiles { get; set; }
    }
}
