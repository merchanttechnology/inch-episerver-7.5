﻿using System;

namespace Ineos.Database.Model
{
    public class LastModified
    {
        public DateTime dateTime { get; set; }
    }
}
