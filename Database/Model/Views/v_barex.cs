using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Views
{
    public class v_barex : BaseEntity, IView
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string grade { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string bu { get; set; }

        [StringLength(100)]
        public string country { get; set; }

        [StringLength(100)]
        public string manufacturingsite { get; set; }

        [StringLength(100)]
        public string brand { get; set; }

        [StringLength(100)]
        public string material { get; set; }

        [StringLength(100)]
        public string submaterial { get; set; }

        [StringLength(255)]
        public string industry { get; set; }

        [StringLength(255)]
        public string subindustry { get; set; }

        public string description { get; set; }

        [StringLength(100)]
        public string owner { get; set; }

        public string applications { get; set; }

        [Key]
        [Column(Order = 2)]
        public bool online { get; set; }

        [StringLength(100)]
        public string mfr { get; set; }

        [StringLength(100)]
        public string density { get; set; }
    }
}
