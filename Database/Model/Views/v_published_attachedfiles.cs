using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ineos.Database.Model.Views
{
    public class v_published_attachedfiles : BaseEntity, IView
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string grade { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string bu { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(100)]
        public string documenttype { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(5)]
        public string documentlanguage { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(100)]
        public string filename { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(100)]
        public string mimetype { get; set; }

        [Key]
        [Column(Order = 6)]
        public string documentcontent { get; set; }
    }
}
