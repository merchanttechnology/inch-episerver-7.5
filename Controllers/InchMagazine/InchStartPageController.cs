﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;
using Ineos.Helpers;
using Ineos.Models.Pages.InchMagazine;

namespace Ineos.Controllers
{
    public class InchStartPageController : PageController<InchStartPage>
    {
        public ActionResult Index(InchStartPage currentPage)
        {
            return View(MagazineHelper.GetIssues(currentPage));
        }
    }
}
