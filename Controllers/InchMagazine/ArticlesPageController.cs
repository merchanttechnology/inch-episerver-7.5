﻿using System.Web.Mvc;
using Ineos.Helpers;
using Ineos.Models.Pages.InchMagazine;
using EPiServer.Web.Mvc;
using Ineos.Models.ViewModels.InchMagazine;

namespace Ineos.Controllers
{
    public class ArticlesPageController : PageController<ArticlesPage>
    {
        public ActionResult Index(ArticlesPage currentPage)
        {
            /* Implementation of action. You can create your own view model class that you pass to the view or
             * you can pass the page type for simpler templates */

            ArticlesModel articleModels = MagazineHelper.GetAllArticlesModel(currentPage);

            return View(articleModels);
        }

    }
}