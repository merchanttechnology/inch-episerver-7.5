﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;
using Ineos.Helpers;
using Ineos.Models.Pages.InchMagazine;

namespace Ineos.Controllers
{
    public class IssuePageController : PageController<IssuePage>
    {
        public ActionResult Index(IssuePage currentPage)
        {
            return View(MagazineHelper.GetAllArticlesModel(currentPage));
        }
    }
}