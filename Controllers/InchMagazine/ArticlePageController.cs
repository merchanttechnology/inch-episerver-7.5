﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;
using Ineos.Helpers;
using Ineos.Models.Pages.InchMagazine;

namespace Ineos.Controllers
{
    public class ArticlePageController : PageController<ArticlePage>
    {
        public ActionResult Index(ArticlePage currentPage)
        {
            return View(MagazineHelper.GetArticleModel(currentPage));
        }
    }
}