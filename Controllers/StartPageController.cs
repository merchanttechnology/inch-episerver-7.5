﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;
using Ineos.Helpers;
using Ineos.Models.Pages;

namespace Ineos.Controllers
{
    public class StartPageController : PageController<StartPage>
    {
        public ActionResult Index(StartPage currentPage)
        {
            return View(MagazineHelper.GetIssues(currentPage));
        }
    }
}
