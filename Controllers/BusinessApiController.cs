﻿using System.Collections.Generic;
using System.Web.Http;
using Ineos.Helpers;
using Ineos.Models.ViewModels.Ineos;

namespace Ineos.Controllers
{
    public class BusinessApiController : ApiController
    {
        //url => .../api/BusinessApi/
        public BusinessListViewModel Get()
        {
            var list = new BusinessListViewModel {BusinessList = new List<BusinessViewModel>()};

            foreach (var bu in DatabaseHelper.GetBusinesses())
            {
                list.BusinessList.Add(new BusinessViewModel { BusinessName = bu });
            }

            return list;
        }
    }
}
