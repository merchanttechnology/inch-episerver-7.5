﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Pages
{
    [ContentType(DisplayName = "Ineos Base Page Data", GUID = "63606c8d-fca0-47a3-87e2-4593141f698f", Description = "NOT FOR USE ON EDIT MODE!!")]
    public class IneosBasePageData : SitePageData
    {
        [CultureSpecific]
        [Display(
            Name = "Visible in Menu",
            Description = "Visible in Menu",
            GroupName = SystemTabNames.Content,
            Order = 10)]
        public virtual bool VisibleMenu { get; set; }
    }
}