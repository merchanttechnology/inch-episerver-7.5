﻿using EPiServer.DataAnnotations;

namespace Ineos.Models.Pages.InchMagazine
{
    [ContentType(DisplayName = "Articles", GUID = "f72ec309-3417-4c10-8dca-4c71bb0c7c6e", Description = "")]
    [ImageUrl("~/Static/images/CMS_Icons/documents.png")]
    [AvailableContentTypes(Include = new[] { typeof(IssuePage) })]
    public class ArticlesPage : SitePageData
    {
    }
}