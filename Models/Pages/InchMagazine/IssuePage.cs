﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Ineos.Models.Pages.InchMagazine
{
    [ContentType(DisplayName = "Issue", GUID = "a1344988-ab35-4fdf-a002-ac5f8609ca11", Description = "An Issue, A Container for all Related Articles within an Issue")]
    [ImageUrl("~/Static/images/CMS_Icons/book.png")]
    [AvailableContentTypes(Include = new[] { typeof(ArticlePage) })]
    public class IssuePage : SitePageData
    {
        [Display(
            Name = "Issue Number",
            Description = "Issue number.",
            GroupName = SystemTabNames.Content,
            Order = 100)]
        public virtual int IssueNumber { get; set; }

        [CultureSpecific]
        [Display(
            GroupName = SystemTabNames.Content,
            Name = "Issue Description",
            Description = "The main body will be shown in the main content area of the page, using the XHTML-editor you can insert for example text, images and tables.",
            Order = 110)]
        public virtual XhtmlString IssueDescription { get; set; }

        [CultureSpecific]
        [Display(
            GroupName = SystemTabNames.Content,
            Name = "Issue Date",
            Description = "Issue Date.",
            Order = 120)]
        public virtual string IssueDate { get; set; }

        [CultureSpecific]
        [Display(
            GroupName = SystemTabNames.Content,
            Name = "Issue Title",
            Description = "Issue Title.",
            Order = 130)]
        [UIHint(UIHint.Textarea)]
        public virtual string IssueTitle { get; set; }

        [CultureSpecific]
        [Display(
            GroupName = SystemTabNames.Content,
            Name = "Issue Subtitle",
            Description = "Issue Subtitle.",
            Order = 140)]
        [UIHint(UIHint.Textarea)]
        public virtual string IssueSubtitle { get; set; }

        [CultureSpecific]
        [Display(
            GroupName = SystemTabNames.Content,
            Name = "Issue PDF Upload",
            Description = "Issue PDF Upload.",
            Order = 150)]
        public virtual ContentReference IssueUploadPdf { get; set; }
    }
}