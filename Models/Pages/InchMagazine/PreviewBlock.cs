﻿using EPiServer.Core;

namespace Ineos.Models.Pages.InchMagazine
{
    public class PreviewBlock : PageData
    {
        public IContent PreviewContent { get; set; }
        public ContentArea ContentArea { get; set; }

        public PreviewBlock(PageData currentPage, IContent previewContent)
            : base(currentPage)
        {
            PreviewContent = previewContent;
            ContentArea = new ContentArea();
            ContentArea.Items.Add(new ContentAreaItem
            {
                ContentLink = PreviewContent.ContentLink
            });
        }
    }
}