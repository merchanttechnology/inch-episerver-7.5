﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Ineos.Business.CustomTypes;

namespace Ineos.Models.Pages.InchMagazine
{
    [ContentType(DisplayName = "Article", GUID = "0096cb8b-b47b-4e4a-aba5-95c9af11e6b4", Description = "")]
    [ImageUrl("~/Static/images/CMS_Icons/article.png")]
    public class ArticlePage : SitePageData
    {
        [CultureSpecific]
        [Display(
            Name = "Header Text",
            Description = "Header Text.",
            GroupName = SystemTabNames.Content,
            Order = 100)]
        [UIHint(UIHint.Textarea)]
        public virtual string HeaderText { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Header Description",
            Description = "Header Description.",
            GroupName = SystemTabNames.Content,
            Order = 110)]
        [UIHint(UIHint.Textarea)]
        public virtual string HeaderDescription { get; set; }

        [Display(
            Name = "Is Featured",
            Description = "Is the article featured, appearance will be different for featured articles.",
            GroupName = SystemTabNames.Content,
            Order = 120)]
        public virtual bool IsFeatured { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Featured Text",
            Description = "Featured text, text will appear in the featured flag.",
            GroupName = SystemTabNames.Content,
            Order = 130)]
        public virtual string FeaturedText { get; set; }

        [Display(
            Name = "Article Size",
            Description = "This will determain is the article genrated is small/medium/large",
            GroupName = SystemTabNames.Content,
            Order = 150)]
        [BackingType(typeof(PropertyNumber))]
        [CustomEnum(typeof(ArticleSize))]
        public virtual ArticleSize ArticleSize { get; set; }

        [Display(
            Name = "Related Content Area",
            Description = "Drag and Drop Content (Blocks/images..) into this area",
            GroupName = SystemTabNames.Content, 
            Order = 160)]
        [CultureSpecific]
        public virtual ContentArea ArticleContentContentArea { get; set; }

        [Display(
            GroupName = Global.GroupNames.MetaData,
            Order = 170)]
        [CultureSpecific]
        public virtual int ReadingTime { get; set; }

        [Display(
            Name = "Is Introduction Article",
            Description = "Only one article per issue can be introduction",
            GroupName = Global.GroupNames.MetaData,
            Order = 180)]
        public virtual bool IsIntroductionArticle { get; set; }

        [Display(
            Name = "Tags List",
            Description = "Tags List",
            GroupName = Global.GroupNames.MetaData,
            Order = 190)]
        [CultureSpecific]
        public virtual string Tags { get; set; }

        [Display(
            Name = "Show Heading",
            Description = "If enabled the articles will not show the header image and text",
            GroupName = Global.GroupNames.MetaData,
            Order = 200)]
        [CultureSpecific]
        public virtual bool ShowHeading { get; set; }

        [Display(
            Name = "Is Article Headline?",
            Description = "if headline article then article will only be available as headline",
            GroupName = Global.GroupNames.MetaData,
            Order = 210)]
        public virtual bool IsHeadline { get; set; }

        [Display(
            Name = "Display on Issue Featured Articles",
            Description = "If ticked, article will be listed on the issue view",
            GroupName = Global.GroupNames.MetaData,
            Order = 220)]
        public virtual bool IssueFeatured { get; set; }
    }
}