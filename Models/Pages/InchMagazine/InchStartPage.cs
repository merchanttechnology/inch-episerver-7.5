﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Ineos.Models.Pages.InchMagazine
{
    [ContentType(
        DisplayName = "Inch StartPage",
        GUID = "2a7fe5c5-5c3e-4230-bf6c-27522da78646",
        Description = "The Home Page - Inch Magazine")]
    [ImageUrl("~/Static/images/CMS_Icons/home.png")]
    [AvailableContentTypes(Include = new[] { typeof(ArticlesPage) })]
    public class InchStartPage : SitePageData
    {
        [Display(
            GroupName = SystemTabNames.Content,
            Order = 200)]
        [CultureSpecific]
        public virtual ContentArea MainContentArea { get; set; }

        [Display(GroupName = SystemTabNames.Content,
            Order = 240)]
        [CultureSpecific]
        public virtual XhtmlString MainBody { get; set; }

        [Display(GroupName = SystemTabNames.Content,
            Order = 350)]
        public virtual PageReference ArticlePageLink { get; set; }

        [Display(GroupName = SystemTabNames.Content,
            Order = 360)]
        public virtual PageReference IssuePageLink { get; set; }

        #region Contact

        [Display(
            Name = "Footer Heading",
            GroupName = Global.GroupNames.Contact,
            Order = 390)]
        [CultureSpecific]
        [UIHint(UIHint.LongString)]
        public virtual string FooterHeading { get; set; }

        [Display(
            Name = "Spread The Word Text",
            GroupName = Global.GroupNames.Contact,
            Order = 400)]
        [CultureSpecific]
        public virtual XhtmlString SpreadTheWordTextContent { get; set; }

        [Display(
            Name = "Editor Name",
            GroupName = Global.GroupNames.Contact,
            Order = 410)]
        [UIHint(UIHint.LongString)]
        [CultureSpecific]
        public virtual string EditorName { get; set; }

        [Display(
            GroupName = Global.GroupNames.Contact,
            Order = 430)]
        [CultureSpecific]
        [UIHint(UIHint.LongString)]
        public virtual string Publisher { get; set; }

        [Display(
            Name = "Editorial Publisher",
            GroupName = Global.GroupNames.Contact,
            Order = 440)]
        [CultureSpecific]
        [UIHint(UIHint.LongString)]
        public virtual string EditorialAddress { get; set; }

        [Display(
            GroupName = Global.GroupNames.Contact,
            Order = 450)]
        [CultureSpecific]
        [UIHint(UIHint.LongString)]
        public virtual string Email { get; set; }

        [Display(GroupName = Global.GroupNames.Contact,
            Order = 460)]
        [CultureSpecific]
        [UIHint(UIHint.LongString)]
        public virtual string Photography { get; set; }

        [Display(
            Name = "Copyright Text",
            GroupName = Global.GroupNames.Contact,
            Order = 470)]
        [CultureSpecific]
        public virtual XhtmlString CopyrightText { get; set; }

        [Display(
            Name = "Footer Logo",
            GroupName = Global.GroupNames.Contact,
            Order = 480)]
        public virtual ContentReference FooterLogo { get; set; }

        [Display(
            Name = "Secondary Logo",
            GroupName = Global.GroupNames.Contact,
            Order = 490)]
        public virtual ContentReference SecondaryLogo { get; set; }

        #endregion

        [Display(
            GroupName = SystemTabNames.Content,
            Order = 500)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference PageFallbackImage { get; set; }

    }
}