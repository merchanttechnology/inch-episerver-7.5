﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Ineos.Models.Properties;

namespace Ineos.Models.Pages
{
    [ContentType(DisplayName = "SitePageData", GUID = "ceb133e6-d80b-44ed-b451-e36193aae8da", Description = "NOT FOR USE ON EDIT MODE!!")]
    public class SitePageData : PageData
    {
        [Display(
            GroupName = Global.GroupNames.MetaData,
            Order = 1)]
        [CultureSpecific]
        public virtual string MetaTitle
        {
            get
            {
                var metaTitle = this.GetPropertyValue(p => p.MetaTitle);

                // Use explicitly set meta title, otherwise fall back to page name
                return !string.IsNullOrWhiteSpace(metaTitle)
                       ? metaTitle
                       : PageName;
            }
            set { this.SetPropertyValue(p => p.MetaTitle, value); }
        }

        [Display(
            GroupName = Global.GroupNames.MetaData,
            Order = 2)]
        [CultureSpecific]
        [BackingType(typeof(PropertyStringList))]
        public virtual string[] MetaKeywords { get; set; }

        [Display(
            GroupName = Global.GroupNames.MetaData,
            Order = 3)]
        [CultureSpecific]
        public virtual string MetaDescription { get; set; }

        [Display(
            GroupName = SystemTabNames.Content,
            Order = 4)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference PageImage { get; set; }
    }
}