﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Pages.Ineos
{
    [ContentType(DisplayName = "History", GUID = "797d8504-2c05-43e5-9efc-6bce6e30796c", Description = "History Container Page")]
    public class HistoryPage : IneosBasePageData
    {
        [CultureSpecific]
        [Display(
            Name = "Header Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual ContentArea HeaderContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Footer Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual ContentArea FooterContentArea { get; set; }
    }
}