﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Pages.Ineos
{
    [ContentType(DisplayName = "Businesses", GUID = "c39164f5-b328-4911-a738-3c4d4713bb12", Description = "Businesses Container Page")]
    public class BusinessesPage : IneosBasePageData
    {
        [CultureSpecific]
        [Display(
            Name = "Header Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual ContentArea HeaderContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Footer Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual ContentArea FooterContentArea { get; set; }
    }
}