﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Pages.Ineos
{
    [ContentType(DisplayName = "Contact Container Page", GUID = "af63fd96-2569-4f29-99d4-3a9ccdf8e47a", Description = "Contact Container Page")]
    public class ContactContainerPage : IneosBasePageData
    {
        [CultureSpecific]
        [Display(
            Name = "Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual ContentArea HeaderContentArea { get; set; }
    }
}