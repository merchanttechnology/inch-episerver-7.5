﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Pages.Ineos
{
    [ContentType(DisplayName = "Stacked", GUID = "57dae6a6-0135-4e46-a521-06c475c16181", Description = "Generic Stacked Page")]
    public class StackedPage : IneosBasePageData
    {
        [CultureSpecific]
        [Display(
            Name = "Main Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual ContentArea MainContentArea { get; set; }
    }
}