﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Pages.Ineos
{
    [ContentType(DisplayName = "Product Search Results Page", GUID = "f24aa534-698e-4e7f-812a-c1d096db1a19", Description = "Product Search Results Page")]
    public class ProductSearchResultsPage : IneosBasePageData
    {
        [CultureSpecific]
        [Display(
            Name = "Main Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual ContentArea HeaderContentArea { get; set; }
    }
}