﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Pages.Ineos
{
    [ContentType(DisplayName = "News Container", GUID = "5fbf9769-c6c4-49ca-83f6-8d84f7644b6e", Description = "News Container Page")]
    public class NewsContainerPage : IneosBasePageData
    {
        [CultureSpecific]
        [Display(
            Name = "Main Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual ContentArea HeaderContentArea { get; set; }

        //[CultureSpecific]
        //[Display(
        //    Name = "Main Content Area",
        //    Description = Global.Constants.ContentAreaPageDescription,
        //    GroupName = SystemTabNames.Content,
        //    Order = 20)]
        //public virtual ContentArea FooterContentArea { get; set; }
    }
}