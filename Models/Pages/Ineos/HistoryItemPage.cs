﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Pages.Ineos
{
    [ContentType(DisplayName = "History Item", GUID = "9395d63d-e3ba-487b-abb2-c8cfcdf03e5e", Description = "History Item")]
    public class HistoryItemPage : IneosBasePageData
    {
        [CultureSpecific]
        [Display(
            Name = "Main Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual ContentArea MainContentArea { get; set; }
    }
}