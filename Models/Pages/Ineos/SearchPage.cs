﻿using EPiServer.DataAnnotations;

namespace Ineos.Models.Pages.Ineos
{
    [ContentType(DisplayName = "Search Page", GUID = "5984f46d-4bc3-4193-b26f-9331b2976ff1", Description = "Site Search Page")]
    public class SearchPage : IneosBasePageData
    {
    }
}