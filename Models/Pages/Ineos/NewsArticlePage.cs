﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Pages.Ineos
{
    [ContentType(DisplayName = "News Article", GUID = "403f606c-b638-4ec5-ad3a-d6254ab6d93c", Description = "Ineos News Article Page")]
    public class NewsArticlePage : IneosBasePageData
    {
        [CultureSpecific]
        [Display(
            Name = "Main Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual ContentArea HeaderContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Side Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual ContentArea SideContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Side Content Area",
            Description = "The Main Content Area, Drag and drop block/partial pages here.",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        public virtual string Heading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Side Content Area",
            Description = "The Main Content Area, Drag and drop block/partial pages here.",
            GroupName = SystemTabNames.Content,
            Order = 50)]
        public virtual PageReference BusinessGroup { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Side Content Area",
            Description = "The Main Content Area, Drag and drop block/partial pages here.",
            GroupName = SystemTabNames.Content,
            Order = 60)]
        public virtual XhtmlString Content { get; set; }
    }
}