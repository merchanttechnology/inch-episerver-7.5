﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Pages.Ineos
{
    [ContentType(DisplayName = "SitesContainerPage", GUID = "0e37874a-936c-454a-b4af-53789202eb0f", Description = "Sites Container Page")]
    public class SitesContainerPage : IneosBasePageData
    {
        [CultureSpecific]
        [Display(
            Name = "Main Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual ContentArea MainContentArea { get; set; }
    }
}