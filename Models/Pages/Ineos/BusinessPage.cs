﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Pages.Ineos
{
    [ContentType(DisplayName = "Business", GUID = "2061c100-23e2-47f8-bbc6-1795233a62c5", Description = "Business Container Page")]
    public class BusinessPage : IneosBasePageData
    {
        #region Business Content
        //[CultureSpecific]
        //[Display(
        //    Name = "Header Content Area",
        //    Description = "Top of page content area, drag partical pages/blocks here",
        //    GroupName = SystemTabNames.Content,
        //    Order = 10)]
        //public virtual ContentArea HeadingContentArea { get; set; }

        [Display(
            Name = "Business Logo",
            Description = "business Logo",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual ContentReference BusinessLogo { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Business Title",
            Description = "The Main Header Title of the busness",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Business Sub Title",
            Description = "Business Sub Title",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        public virtual string SubTitle { get; set; }

        //[CultureSpecific]
        //[Display(
        //    Name = "Footer Content Area",
        //    Description = "Bottom of Page Content Area",
        //    GroupName = SystemTabNames.Content,
        //    Order = 50)]
        //public virtual ContentArea FooterContentArea { get; set; } 
        #endregion

        #region Business Unit
        [CultureSpecific]
        [Display(
            Name = "Business Title",
            Description = "Business Unit - Business Title",
            GroupName = Global.GroupNames.BusinessUnit,
            Order = 60)]
        public virtual string BusinessUnitBusinessTitle { get; set; }
        [CultureSpecific]
        [Display(
            Name = "Business Content",
            Description = "Business Unit - Business Content",
            GroupName = Global.GroupNames.BusinessUnit,
            Order = 70)]
        public virtual string BusinessUnitBusinessDescription { get; set; }
        [Display(
            Name = "Business Image",
            Description = "Business Unit - Business Image",
            GroupName = SystemTabNames.Content,
            Order = 80)]
        public virtual ContentReference BusinessUnitBusinessImage { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Pedigree Title",
            Description = "Business Unit - Pedigree Title",
            GroupName = Global.GroupNames.BusinessUnit,
            Order = 90)]
        public virtual string BusinessUnitPedigreeTitle { get; set; }
        [CultureSpecific]
        [Display(
            Name = "Pedigree Content",
            Description = "Business Unit - Pedigree Content",
            GroupName = Global.GroupNames.BusinessUnit,
            Order = 100)]
        public virtual string BusinessUnitPedigreeDescription { get; set; }
        [Display(
            Name = "Pedigree Image",
            Description = "Business Unit - Pedigree Image",
            GroupName = SystemTabNames.Content,
            Order = 110)]
        public virtual ContentReference BusinessUnitPedigreeImage { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Brands/Products Title",
            Description = "Business Unit - Brands/Products Title",
            GroupName = Global.GroupNames.BusinessUnit,
            Order = 120)]
        public virtual string BusinessUnitBrandsTitle { get; set; }
        [CultureSpecific]
        [Display(
            Name = "Brands/Products Content",
            Description = "Business Unit - Brands/Products Content",
            GroupName = Global.GroupNames.BusinessUnit,
            Order = 130)]
        public virtual string BusinessUnitBrandsDescription { get; set; }
        [Display(
            Name = "Brands/Products Image",
            Description = "Business Unit - Brands/Products Image",
            GroupName = SystemTabNames.Content,
            Order = 140)]
        public virtual ContentReference BusinessUnitBrandsImage { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Markets Title",
            Description = "Business Unit - Markets Title",
            GroupName = Global.GroupNames.BusinessUnit,
            Order = 150)]
        public virtual string BusinessUnitKeyMarketsTitle { get; set; }
        [CultureSpecific]
        [Display(
            Name = "Markets Content",
            Description = "Business Unit - Markets Content",
            GroupName = Global.GroupNames.BusinessUnit,
            Order = 160)]
        public virtual string BusinessUnitKeyMarketsDescription { get; set; }
        [Display(
            Name = "Business Image",
            Description = "Business Unit - Markets Image",
            GroupName = SystemTabNames.Content,
            Order = 170)]
        public virtual ContentReference BusinessUnitKeyMarketsImage { get; set; }
        #endregion
    }
}