﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Pages
{
    [ContentType(
        DisplayName = "StartPage",
        GUID = "E1E56DF1-5E5B-4F1C-B9C9-394E7D55B4DF",
        Description = "The Home Page - Ineos")]
    [ImageUrl("~/Static/images/CMS_Icons/home.png")]
    public class StartPage : IneosBasePageData
    {
        //[Display(GroupName = SystemTabNames.Content, Order = 100)]
        //[BackingType(typeof(PropertyKeyValueItems))]
        //[KeyValueItems("Name", "E-mail", "Add", "X", "", "", @"^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", "The entered value is not valid. Must be a valid e-mail.")]
        //public virtual IEnumerable<KeyValueItem> KeyValueItems { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Main Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual ContentArea MainContentArea { get; set; }
    }
}