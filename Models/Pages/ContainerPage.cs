﻿using EPiServer.DataAnnotations;
using Ineos.Business.Rendering;

namespace Ineos.Models.Pages
{
    [ContentType(GUID = "73462f5e-42ea-4e2d-add8-2e98479cc1fc", DisplayName = Global.GroupNames.Specialized)]
    public class ContainerPage : SitePageData, IContainerPage
    {
    }
}