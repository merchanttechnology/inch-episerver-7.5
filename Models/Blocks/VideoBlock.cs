﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Ineos.Models.Blocks
{
    [ContentType(DisplayName = "Video Block", GUID = "fbbdad4b-bbc0-4af6-9534-6716150e73b2", Description = "Video Block")]
    [ImageUrl("~/Static/images/CMS_Icons/video.png")]
    public class VideoBlock : SiteBlockData
    {

        [CultureSpecific]
        [Display(
            Name = "Video URL",
            Description = "Video URL",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual string VideoUrl { get; set; }

        [Display(
            GroupName = SystemTabNames.Content,
            Name = "Video Watermark Image",
            Description = "Video Watermark Image.",
            Order = 30)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference VideoWatermarkImage { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Video Block Text",
            Description = "Video Block Text",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        public virtual String VideoBlockText { get; set; }
    }
}