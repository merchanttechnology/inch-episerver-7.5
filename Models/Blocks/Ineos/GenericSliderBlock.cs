﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Generic Slider Block", GUID = "fe458aef-6eb6-47e4-be81-3d34aa1f24f3", Description = "Generic Slider Block")]
    public class GenericSliderBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Slider Heading",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual string SliderHeading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Background Colour",
            Description = "HEX decimal background colour",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual string SliderBackgroundColour { get; set; }

        [Display(
            Name = "Display Heading ?",
            Description = "Tick to Display Heading, leave unticked to hide heading",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        public virtual bool DisplayHeading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Main Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 50)]
        public virtual ContentArea MainContentArea { get; set; }
    }
}