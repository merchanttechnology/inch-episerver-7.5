﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Ineos.Business.CustomTypes;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Product Header", GUID = "b5782db6-b398-41af-9586-466eb33646d1", Description = "Product Header Block")]
    public class ProductHeaderBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Title",
            Description = "Header Title",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string Title { get; set; }

        [Display(
            Name = "Header Type",
            Description = "Header Type, what will the header be used for.",
            GroupName = SystemTabNames.Content,
            Order = 2)]
        [BackingType(typeof(PropertyNumber))]
        [CustomEnum(typeof(HeaderBlockType))]
        public virtual HeaderBlockType ArticleSize { get; set; }

        [Display(
            GroupName = SystemTabNames.Content,
            Name = "Background Image",
            Description = "Background Image.",
            Order = 3)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Background { get; set; }
    }
}

