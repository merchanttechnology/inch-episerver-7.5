﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "LinkBlockContainer", GUID = "fb8cbeaf-36f5-4d69-93e1-bc4fde876fb2", Description = "Container for link blocks")]
    public class LinksContainerBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Heading",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual string Heading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Description",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        [UIHint(UIHint.LongString)]
        public virtual string Description { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Block Link",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        public virtual string BlockLink { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Scrollable Block",
            Description = "Add scrollbars to the container",
            GroupName = SystemTabNames.Content,
            Order = 50)]
        public virtual string ScrollableBlock { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Main Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 60)]
        public virtual ContentArea MainContentArea { get; set; }
    }
}