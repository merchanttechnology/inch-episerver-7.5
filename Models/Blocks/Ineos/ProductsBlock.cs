﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Products Block", GUID = "67cb5fe6-80ad-4845-b09d-1807042e6689", Description = "Products Block")]
    public class ProductsBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Title",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Background Image",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual ContentReference BackgroundImage { get; set; }
    }
}