﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Ineos.Business.CustomTypes;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Generic Carousel", GUID = "060be8d7-7d04-4bb9-a9de-f49f983f4ec1", Description = "Generic Carousel Block")]
    public class GenericCarouselBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Header Text",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual string HeaderText { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual string Link { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Notification Placeholder",
            Description = "Drag and drop notification block here.",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        public virtual ContentArea NoificationContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Background Image",
            GroupName = SystemTabNames.Content,
            Order = 50)]
        public virtual string BackgroundImage { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Carousel Placeholder Content Area",
            Description = "Drag and drop carousel item blocks here.",
            GroupName = SystemTabNames.Content,
            Order = 60)]
        public virtual ContentArea CarouselPlaceHolder { get; set; }

        [Display(
            Name = "CarouselType",
            Description = "Type of carousel, different types render differently",
            GroupName = SystemTabNames.Content,
            Order = 70)]
        [BackingType(typeof(PropertyNumber))]
        [CustomEnum(typeof(CarouselType))]
        public virtual CarouselType CarouselType { get; set; }
    }
}