﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Tab Content", GUID = "dfd57087-00bd-4733-8d55-6e30272dcd70", Description = "Tab Content Block")]
    public class TabContentBlock : BlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Heading/Tab Name",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual string Heading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Name",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual XhtmlString ContentText { get; set; }
    }
}