﻿using EPiServer.DataAnnotations;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Markets Block", GUID = "d6ffae6e-e003-40c5-9be7-0a5e2c73580f")]
    public class MarketsBlock : SiteBlockData
    {
    }
}