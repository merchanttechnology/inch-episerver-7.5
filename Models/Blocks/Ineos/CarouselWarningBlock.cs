﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Ineos.Business.CustomTypes;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Carousel Warning", GUID = "3e1b23fa-245f-422c-8aec-876378f22529", Description = "Carousel Warning Block")]
    public class CarouselWarningBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Warning Header",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual string WarningHeader { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Warning Date",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual DateTime WarningDate { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Warning Description",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        [UIHint(UIHint.LongString)]
        public virtual string WarningDescription { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link",
            GroupName = SystemTabNames.Content,
            Order = 50)]
        public virtual string Link { get; set; }

        [Display(
            Name = "Carousel Warning Type",
            Description = "Type of carousel warning, Error/Warning/Information or OFF",
            GroupName = SystemTabNames.Content,
            Order = 60)]
        [BackingType(typeof(PropertyNumber))]
        [CustomEnum(typeof(CarouselWarningType))]
        public virtual CarouselWarningType CarouselWarningType { get; set; }
    }
}