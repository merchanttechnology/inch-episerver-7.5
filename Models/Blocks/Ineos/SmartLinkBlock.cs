﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Smart Link", GUID = "3567a429-1d32-4855-a265-0420be2777a5", Description = "Smart Link Block")]
    public class SmartLinkBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Block Link",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual string BlockLink { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Main Content Area",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual ContentArea MainContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Description",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        [UIHint(UIHint.LongString)]
        public virtual string Description { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Make Text Link ?",
            Description = "If ticked, the text will be a link, specifiy the destination in the field ContentArea",
            GroupName = SystemTabNames.Content,
            Order = 50)]
        public virtual bool MakeTextLink { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Content Link",
            Description = "Text Link Destination",
            GroupName = SystemTabNames.Content,
            Order = 60)]
        public virtual ContentReference ContentLink { get; set; }
    }
}