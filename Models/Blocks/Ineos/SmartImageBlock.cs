﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Smart Image", GUID = "b4089c94-43ea-4fef-ac54-00b86ca241a1", Description = "Smart Image Block")]
    public class SmartImageBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Main Content Area",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual ContentArea MainContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Base Watermark Text",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual ContentArea ImageBaseWatermark { get; set; }
    }
}