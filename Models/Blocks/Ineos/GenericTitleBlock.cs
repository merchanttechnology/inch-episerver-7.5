﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Generic Title", GUID = "cf12dde7-144a-40e7-957a-479c32954d1d", Description = "Generic Title Block")]
    public class GenericTitleBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Heading",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual string Heading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Sub-Heading",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual string SubHeading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Content Text",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        public virtual string ContentText { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Background Colour",
            GroupName = SystemTabNames.Content,
            Order = 50)]
        public virtual string BackgroundColour { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Background Image",
            GroupName = SystemTabNames.Content,
            Order = 60)]
        public virtual ContentReference BackgroundImage { get; set; }
    }
}