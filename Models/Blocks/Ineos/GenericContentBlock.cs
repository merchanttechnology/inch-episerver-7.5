﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Generic Content", GUID = "6a99a5ee-ef1e-49d5-b6c0-ed86c8d76c3a", Description = "Generic Content Area Block")]
    public class GenericContentBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Heading",
            Description = "Heading Field",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual string Heading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Description",
            Description = "Description Field",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        [UIHint(UIHint.LongString)]
        public virtual string Description { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 40)]
        public virtual ContentArea ContentArea { get; set; }
    }
}