﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Slider Item", GUID = "b3351bbb-12bc-421f-98c2-5e41898df44c", Description = "Slider Item Block")]
    public class SliderItemBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Slider Heading",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual string SliderHeading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Slider Content",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual XhtmlString SliderContent { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Is Introduction?",
            Description = "Introduction slide will be rendered differently from other slides",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        public virtual bool IsIntroduction { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Sub-Heading",
            GroupName = SystemTabNames.Content,
            Order = 50)]
        [UIHint(UIHint.LongString)]
        public virtual string SubSubHeading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Slider Item Image",
            GroupName = SystemTabNames.Content,
            Order = 60)]
        public virtual ContentReference SliderItemImage { get; set; }
    }
}