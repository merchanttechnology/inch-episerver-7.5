﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Ineos.Business.CustomTypes;

namespace Ineos.Models.Blocks.Ineos
{
    //TODO: speak to cos how some filters are implemented, check paper notes on notepad
    [ContentType(
        DisplayName = "Header Bar", 
        GUID = "0b5842d4-04f2-4e3e-8b2d-6143b766ebf5", 
        Description = "Generic Header Block")]
    public class HeaderBarBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Header",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual string Header { get; set; }

        [Display(
            Name = "Header Type",
            Description = "Header Type, different types render differently",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        [BackingType(typeof(PropertyNumber))]
        [CustomEnum(typeof(HeaderBlockType))]
        public virtual HeaderBlockType HeaderType { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Header",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        public virtual string Background { get; set; }
    }
}