﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Carousel Item", GUID = "9f825e94-6424-44e4-94c7-d2a6c5e55c7e", Description = "Carousel Item Block")]
    public class CarouselItemBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Main Content Area",
            Description = Global.Constants.ContentAreaPageDescription,
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual ContentArea MainContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link Text",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual string LinkText { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Link",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        public virtual ContentReference Link { get; set; }
    }
}