﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Generic Free Text", GUID = "a3b85c13-bcf0-42d2-929e-a7f805fc8d29", Description = "Generic Free Text Block")]
    public class GenericFreeTextBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Main Content",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual XhtmlString MainContent { get; set; }

        [Display(
            Name = "BackgroundImage",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual ContentReference BackgroundImage { get; set; }
    }
}