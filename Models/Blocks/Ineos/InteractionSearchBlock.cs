﻿using EPiServer.DataAnnotations;

namespace Ineos.Models.Blocks.Ineos
{
    [ContentType(DisplayName = "Interaction Search", GUID = "697f04d4-ba52-4779-bcda-5ee701c502b1", Description = "Interaction Search Block")]
    public class InteractionSearchBlock : SiteBlockData
    {
    }
}