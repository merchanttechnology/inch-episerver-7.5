﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Ineos.Models.Blocks.InchMagazine
{
    [ContentType(DisplayName = "Before After Block", GUID = "72949291-ae4b-4d72-9d57-d0a00161c06a", Description = "Before / After Image Block")]
    [ImageUrl("~/Static/images/CMS_Icons/images2.png")]
    public class BeforeAfterBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Before Text",
            Description = "Before Text",
            GroupName = SystemTabNames.Content,
            Order = 10)]
        public virtual XhtmlString BeforeText { get; set; }

        [Display(
            GroupName = SystemTabNames.Content,
            Name = "Before Image",
            Description = "Before Image.",
            Order = 20)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference BeforeImage { get; set; }

        [CultureSpecific]
        [Display(
            Name = "After Text",
            Description = "Before Text",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual XhtmlString AfterText { get; set; }

        [Display(
            GroupName = SystemTabNames.Content,
            Name = "After Image",
            Description = "After Image.",
            Order = 40)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference AfterImage { get; set; }
    }
}