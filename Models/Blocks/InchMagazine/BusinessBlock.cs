﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Ineos.Models.Blocks.InchMagazine
{
    [ContentType(DisplayName = "BusinessBlock", GUID = "c63563da-28a9-4219-b597-d2ac1e93d7a5",
        Description = "Business Block")]
    [ImageUrl("~/Static/images/CMS_Icons/business1.png")]
    public class BusinessBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Business Name",
            Description = "Business Name.",
            GroupName = SystemTabNames.Content,
            Order = 10)]
        public virtual String Name { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Business Block Heading",
            Description = "Business Block Heading.",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual String BusinessBlockHeading { get; set; }

        [CultureSpecific]
        [Display(
            GroupName = SystemTabNames.Content,
            Name = "Business Block Sub-Heading",
            Description = "Business Block Sub-Heading.",
            Order = 30)]
        public virtual String BusinessBlockSubHeading { get; set; }

        [CultureSpecific]
        [Display(
            GroupName = SystemTabNames.Content,
            Name = "Business Block Title",
            Description = "Business Block Title.",
            Order = 40)]
        public virtual String BusinessBlockTitle { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Business Block Text",
            Description = "Business Block Text.",
            GroupName = SystemTabNames.Content,
            Order = 50)]
        public virtual XhtmlString BusinessBlockText { get; set; }

        [Display(
            GroupName = SystemTabNames.Content,
            Name = "Business Block Image",
            Description = "Business Block Image.",
            Order = 60)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference BusinessBlockImage { get; set; }

        [Display(
            GroupName = SystemTabNames.Content,
            Name = "Business Block Background Colour",
            Description = "Business Block Background Colour.",
            Order = 70)]
        public virtual String BusinessBlockBackgroundColour { get; set; }
    }
}