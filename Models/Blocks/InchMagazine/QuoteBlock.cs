﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Blocks.InchMagazine
{
    [ContentType(DisplayName = "QuoteBlock", GUID = "4fb1ca0c-1f2c-48c8-9edb-de25f3531562", Description = "Quote Block")]
    [ImageUrl("~/Static/images/CMS_Icons/quote1.png")]
    public class QuoteBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Quote Heading",
            Description = "Quote Heading",
            GroupName = SystemTabNames.Content,
            Order = 10)]
        public virtual String QuoteHeading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Quote Body",
            Description = "Quote Body.",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual String QuoteString { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Quote Footer",
            Description = "Quote Footer.",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual String QuoteFooter { get; set; }
    }
}