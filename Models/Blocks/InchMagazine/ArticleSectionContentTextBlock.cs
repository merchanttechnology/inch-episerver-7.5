﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Blocks.InchMagazine
{
    [ContentType(
        DisplayName = "Article Section Content With Aside", 
        GUID = "3b10347d-63d7-4a3f-a124-b6abf3954d90", 
        Description = "A Text Content Section, can be dragged to a content area in article"
        )]
    [ImageUrl("~/Static/images/CMS_Icons/article_text_image.png")]
    public class ArticleSectionContentTextBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Section Header",
            Description = "Section Header.",
            GroupName = SystemTabNames.Content,
            Order = 10)]
        public virtual string Introduction { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Article Rich Text Area",
            Description = "Article Rich Text Area.",
            GroupName = SystemTabNames.Content,
            Order = 20)]
        public virtual XhtmlString RichText { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Article Content Area",
            Description = "Article Content Area.",
            GroupName = SystemTabNames.Content,
            Order = 30)]
        public virtual ContentArea ArticleContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Aside Content Area",
            Description = "Aside Content Area.",
            GroupName = SystemTabNames.Content,
            Order = 40)]
        public virtual ContentArea ContentArea { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Article Header",
            Description = "Article Header, can be empty if article header is visible.",
            GroupName = SystemTabNames.Content,
            Order = 50)]
        public virtual string ArticleHeading { get; set; }
    }
}