﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace Ineos.Models.Blocks.InchMagazine
{
    [ContentType(DisplayName = "HeaderBlock", GUID = "7462067e-a20d-4fbd-ac38-437b73f46f74", Description = "")]
    [ImageUrl("~/Static/images/CMS_Icons/header.png")]
    public class HeaderBlock : SiteBlockData
    {
        [Display(
            GroupName = SystemTabNames.Content,
            Order = 1)]
        [CultureSpecific]
        public virtual string Heading { get; set; }

        [Display(
            GroupName = SystemTabNames.Content,
            Order = 2)]
        [CultureSpecific]
        public virtual XhtmlString Body { get; set; }
    }
}