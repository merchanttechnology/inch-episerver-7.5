﻿using EPiServer.Core;

namespace Ineos.Models.Blocks
{
    /// <summary>
    /// Base class for all block types on the site
    /// </summary>
    public class SiteBlockData : BlockData
    {
    }
}