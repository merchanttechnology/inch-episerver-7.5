﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using EPiServer.Core;
using EPiServer.PlugIn;

namespace Ineos.Models.Properties
{
    [PropertyDefinitionTypePlugIn(Description = "A property for list of key-value-items.",
        DisplayName = "Key-Value Items")]
    public class PropertyKeyValueItems : PropertyLongString
    {
        public override Type PropertyValueType
        {
            get { return typeof (IEnumerable<KeyValueItem>); }
        }

        public override object Value
        {
            get
            {
                var value = base.Value as string;
                if (value == null)
                {
                    return null;
                }
                var serializer = new JavaScriptSerializer();
                return serializer.Deserialize(value, typeof (IEnumerable<KeyValueItem>));
            }
            set
            {
                if (value is IEnumerable<KeyValueItem>)
                {
                    var serializer = new JavaScriptSerializer();
                    base.Value = serializer.Serialize(value);
                }
                else
                {
                    base.Value = value;
                }
            }
        }
    }

    public class KeyValueItem
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}