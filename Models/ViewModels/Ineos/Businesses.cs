﻿using System.Collections.Generic;

namespace Ineos.Models.ViewModels.Ineos
{
    public class BusinessListViewModel
    {
        public List<BusinessViewModel> BusinessList { get; set; }
    }

    public class BusinessViewModel
    {
        public string BusinessName { get; set; }
    }

    public class HistoryListViewModel { }
    public class HistoryItemViewModel{ }

    public class NewsArticlesListViewModel { }
    public class NewsArticleViewModel { }

    public class ProductListViewModel { }
    public class ProductViewModel { }

    public class MarketsListViewModel { }
    public class MarketViewModel { }
}