using System.Collections.Generic;
using EPiServer.Core;

namespace Ineos.Models.ViewModels.InchMagazine
{
    /// <summary>
    /// Magazine Issue Model
    /// </summary>
    public class IssueModel
    {
        public int IssueNumber { get; set; }
        public List<string> TopTitles { get; set; }
        public ContentReference IssueImage { get; set; }
        public ContentReference IssueUrl { get; set; }
        public ContentReference IssueDocument { get; set; }
    }
}