using EPiServer.Core;

namespace Ineos.Models.ViewModels.InchMagazine
{
    /// <summary>
    /// Contact Model, Display the footer contacts
    /// </summary>
    public class ContactModel
    {
        public string FooterHeading { get; set; }
        public XhtmlString SpreadTheWordTextContent { get; set; }
        public string EditorName { get; set; }
        public string Publisher { get; set; }
        public string EditorialAddress { get; set; }
        public string Email { get; set; }
        public string Photography { get; set; }
        public XhtmlString CopyrightText { get; set; }
        public ContentReference LogoLink { get; set; }
        public ContentReference SecondaryLogo { get; set; }
    }
}