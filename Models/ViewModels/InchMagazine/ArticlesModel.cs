using System.Collections.Generic;
using EPiServer.Core;
using Ineos.Models.Pages;

namespace Ineos.Models.ViewModels.InchMagazine
{
    public class ArticlesModel : PageViewModel<SitePageData>
    {
        public ArticlesModel(SitePageData page) : base(page) { }
        public List<ArticleModel> Articles { get; set; }
        public KeyValuePair<ContentReference, string> PreviousPage { get; set; }
        public KeyValuePair<ContentReference, string> NextPage { get; set; }
        public ContentReference GenericFallBackBackgroundImage { get; set; }
        public List<RelatedArticleModel> InTheHeadline { get; set; }
        public List<RelatedArticleModel> MoreFromInchArticles { get; set; }
        public List<RelatedArticleModel> InTheIssueArticles { get; set; }
    }
}