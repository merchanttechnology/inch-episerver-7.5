﻿using System;
using System.Collections.Generic;
using EPiServer.Core;
using Ineos.Business.CustomTypes;

namespace Ineos.Models.ViewModels.InchMagazine
{
    /// <summary>
    /// Article Model
    /// </summary>
    public class ArticleModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsFeatured { get; set; }
        public string FeaturedText { get; set; }
        public ContentReference HeaderImage { get; set; }
        public virtual ArticleSize ArticleSize { get; set; }
        public List<VideoModel> Videos { get; set; }
        public string Category { get; set; }
        public List<string> Tags { get; set; }
        public bool IsIntroduction { get; set; }
        public ContentReference PageReference { get; set; }
        public int ReadTime { get; set; }
        public DateTime PublishDateTime { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ArticleContentText { get; set; }
        public int IssueNumber { get; set; }
        public ContentReference IssueDocument { get; set; }
    }
}