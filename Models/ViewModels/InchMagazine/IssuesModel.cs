using System.Collections.Generic;
using Ineos.Models.Pages;

namespace Ineos.Models.ViewModels.InchMagazine
{
    public class IssuesModel : PageViewModel<SitePageData>
    {
        public IssuesModel(SitePageData page) : base(page) { }
        public List<IssueModel> Issues { get; set; }
    }
}