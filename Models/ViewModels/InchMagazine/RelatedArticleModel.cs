using EPiServer.Core;

namespace Ineos.Models.ViewModels.InchMagazine
{
    public class RelatedArticleModel
    {
        public ContentReference PageReference { get; set; }
        public ContentReference HeaderImage { get; set; }
        public string ArticleContentText { get; set; }
        public int ReadTime { get; set; }
        public string HeaderText { get; set; }
        public string HeaderDescription { get; set; }
    }
}