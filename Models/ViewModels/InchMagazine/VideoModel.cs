using EPiServer.Core;

namespace Ineos.Models.ViewModels.InchMagazine
{
    /// <summary>
    /// Article Videos Model
    /// </summary>
    public class VideoModel
    {
        public string VideoUrl { get; set; }
        public ContentReference VideoImageUrl { get; set; }
        public string VideoDescription { get; set; }
    }
}