﻿using EPiServer.Core;
using Ineos.Models.Pages;
using Ineos.Models.Pages.InchMagazine;

namespace Ineos.Models.ViewModels
{
    public class PreviewModel : IPageViewModel<SitePageData> 
    {
        public PreviewModel(PageData currentPage, IContent previewContent)
        {
            PreviewBlock = new PreviewBlock(currentPage, previewContent);
            CurrentPage = currentPage as SitePageData;
        }
        public PreviewBlock PreviewBlock { get; set; }
        public SitePageData CurrentPage { get; set; }
    }
}