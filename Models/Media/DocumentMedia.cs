﻿using System;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Framework.DataAnnotations;

namespace Ineos.Models.Media
{
    [ContentType(GUID = "39EAEB9E-EBA8-4669-8411-B854F29BE23B")]
    [MediaDescriptor(ExtensionString = "pdf,doc,docx")]
    public class DocumentMedia : MediaData
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual int VersionNumber { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual String Description { get; set; }
    }
}