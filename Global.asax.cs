﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;

namespace Ineos
{
    public class Global : EPiServer.Global
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            // register webapi routes
            GlobalConfiguration.Configuration.Routes.MapHttpRoute(
                    "DefaultApi",
                    "api/{controller}/{id}",
                    new { id = RouteParameter.Optional }
                );
        }

        /// <summary>
        /// Group names for content types and properties
        /// </summary>
        public static class GroupNames
        {
            public const string Contact = "Contact";
            public const string Default = "Default";
            public const string MetaData = "Metadata";
            public const string News = "News";
            public const string Products = "Products";
            public const string SiteSettings = "SiteSettings";
            public const string Specialized = "Specialized";
            public const string BusinessUnit = "Business-Unit";
        }

        /// <summary>
        /// Names used for UIHint attributes to map specific rendering controls to page properties
        /// </summary>
        public static class SiteUIHints
        {
            public const string Contact = "contact";
            public const string Strings = "StringList";
        }

        /// <summary>
        /// Tags to use for the main widths used in the Bootstrap HTML framework
        /// </summary>
        public static class ContentAreaTags
        {
            public const string FullWidth = "span12";
            public const string TwoThirdsWidth = "span8";
            public const string HalfWidth = "span6";
            public const string OneThirdWidth = "span4";
            public const string NoRenderer = "norenderer";
        }

        /// <summary>
        /// Main widths used in the Bootstrap HTML framework
        /// </summary>
        public static class ContentAreaWidths
        {
            public const int FullWidth = 12;
            public const int TwoThirdsWidth = 8;
            public const int HalfWidth = 6;
            public const int OneThirdWidth = 4;
        }

        public static Dictionary<string, int> ContentAreaTagWidths = new Dictionary<string, int>
            {
                {ContentAreaTags.FullWidth, ContentAreaWidths.FullWidth},
                {ContentAreaTags.TwoThirdsWidth, ContentAreaWidths.TwoThirdsWidth},
                {ContentAreaTags.HalfWidth, ContentAreaWidths.HalfWidth},
                {ContentAreaTags.OneThirdWidth, ContentAreaWidths.OneThirdWidth}
            };

        public static class Constants
        {
            public static char TagDelimiter = ConfigurationManager.AppSettings["TagDelimiter"].FirstOrDefault();
            public static string ArticleCategoryRoot = "ArticleCategory";
            public static string ArticleCategoryAll = "All";
            public const string ContentAreaPageDescription = "Drag and drop block/partial pages here.";
        }
    }
}