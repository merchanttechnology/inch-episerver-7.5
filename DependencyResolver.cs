using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Dependencies;

namespace Ineos
{
    public class DependencyScope : IDependencyScope
    {
        private readonly IContainer _container;
        public DependencyScope(IContainer container)
        {
            _container = container;
        }

        public object GetService(Type serviceType)
        {
            return serviceType.IsAbstract || serviceType.IsInterface ? _container.TryGetInstance(serviceType)
                : _container.GetInstance(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _container.GetAllInstances(serviceType).Cast<object>();
        }
    

        public void Dispose()
        {
            if (_container != null)
                _container.Dispose();
        }
    }

    public class DependencyResolver : DependencyScope, IDependencyResolver
    {
        private readonly IContainer _container;
        public DependencyResolver(IContainer container) :
            base(container.GetNestedContainer())
        {
            _container = container;
        }

        public IDependencyScope BeginScope()
        {
            // Create a Nested Container.So that we can dispose the _container once the request is completed.
            var nestedContainer = _container.GetNestedContainer();
            return new DependencyScope(nestedContainer);
        }
    }
}