﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Mvc;
using Ineos.Controllers;
using Ineos.Models.Pages;

namespace Ineos.Business.Rendering
{
    //TODO: Cleanup this class
    [ServiceConfiguration(typeof(IViewTemplateModelRegistrator))]
    public class TemplateCoordinator : IViewTemplateModelRegistrator
    {
        public const string GenericBlockFolder = "~/Views/Shared/Blocks/GenericBlocks/";
        public const string InchBlockFolder = "~/Views/Shared/Blocks/InchBlocks/";
        public const string IneosBlockFolder = "~/Views/Shared/Blocks/IneosBlocks/";
        public const string PagePartialsFolder = "~/Views/Shared/PagePartials/";

        /// <summary>
        /// Disable DefaultPageController for page types that shouldn't have any renderer as pages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public static void OnTemplateResolved(object sender, TemplateResolverEventArgs args)
        {
            if (args.ItemToRender is IContainerPage && args.SelectedTemplate != null && args.SelectedTemplate.TemplateType == typeof(DefaultPageController))
            {
                args.SelectedTemplate = null;
            }
        }

        /// <summary>
        /// Registers renderers/templates which are not automatically discovered, 
        /// i.e. partial views whose names does not match a content type's name.
        /// </summary>
        /// <remarks>
        /// Using only partial views instead of controllers for blocks and page partials
        /// has performance benefits as they will only require calls to RenderPartial instead of
        /// RenderAction for controllers.
        /// Registering partial views as templates this way also enables specifying tags and 
        /// that a template supports all types inheriting from the content type/model type.
        /// </remarks>
        public void Register(TemplateModelCollection viewTemplateModelRegistrator)
        {


            viewTemplateModelRegistrator.Add(typeof(SitePageData), new TemplateModel
            {
                Name = "PagePartial",
                Inherited = true,
                AvailableWithoutTag = true,
                Path = PagePartialPath("Page.cshtml")
            });

            viewTemplateModelRegistrator.Add(typeof(SitePageData), new TemplateModel
            {
                Name = "PagePartialWide",
                Inherited = true,
                Tags = new[] { Global.ContentAreaTags.TwoThirdsWidth, Global.ContentAreaTags.FullWidth },
                AvailableWithoutTag = false,
                Path = PagePartialPath("PageWide.cshtml")
            });

            #region Saved Code fo Later
            //viewTemplateModelRegistrator.Add(typeof(JumbotronBlock), new TemplateModel
            //{
            //    Tags = new[] { Global.ContentAreaTags.FullWidth },
            //    AvailableWithoutTag = false,
            //    Path = BlockPath("JumbotronBlockWide.cshtml")
            //});

            //viewTemplateModelRegistrator.Add(typeof(TeaserBlock), new TemplateModel
            //{
            //    Name = "TeaserBlockWide",
            //    Tags = new[] { Global.ContentAreaTags.TwoThirdsWidth, Global.ContentAreaTags.FullWidth },
            //    AvailableWithoutTag = false,
            //    Path = BlockPath("TeaserBlockWide.cshtml")
            //});

            //viewTemplateModelRegistrator.Add(typeof(SitePageData), new TemplateModel
            //{
            //    Name = "PageType",
            //    Inherited = true,
            //    AvailableWithoutTag = true,
            //    Path = PagePath("StartPage.cshtml")
            //});

            //viewTemplateModelRegistrator.Add(typeof(ArticlesPage), new TemplateModel
            //{
            //    Name = "PageType",
            //    Inherited = true,
            //    AvailableWithoutTag = true,
            //    Path = PagePath("ArticlesPage.cshtml")
            //});

            //viewTemplateModelRegistrator.Add(typeof(ContactPage), new TemplateModel
            //{
            //    Name = "ContactPagePartialWide",
            //    Tags = new[] { Global.ContentAreaTags.TwoThirdsWidth, Global.ContentAreaTags.FullWidth },
            //    AvailableWithoutTag = false,
            //    Path = PagePartialPath("ContactPageWide.cshtml")
            //}); 

            //viewTemplateModelRegistrator.Add(typeof(SiteBlockData), new TemplateModel
            //{
            //    Name = "SiteBlockDataNoDivContainerRender",
            //    AvailableWithoutTag = true,
            //    Path = GenericBlockPath("SiteBlockDataNoDivContainerRender.cshtml")
            //});
            #endregion

            viewTemplateModelRegistrator.Add(typeof(IContentData), new TemplateModel
            {
                Name = "NoRendererMessage",
                Inherited = true,
                Tags = new[] { Global.ContentAreaTags.NoRenderer },
                AvailableWithoutTag = false,
                Path = GenericBlockPath("NoRenderer.cshtml")
            });


        }

        private static string GenericBlockPath(string fileName)
        {
            return string.Format("{0}{1}", GenericBlockFolder, fileName);
        }

        private static string PagePartialPath(string fileName)
        {
            return string.Format("{0}{1}", PagePartialsFolder, fileName);
        }

        public static class Tags
        {
            public const string NoWrappersContentArea = "NoWrappersContentArea";
            public const string ImageWrapperContentArea = "ImageContentArea";
        }
    }
}