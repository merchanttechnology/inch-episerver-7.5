﻿using EPiServer;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;

namespace Ineos.Business.Initialization
{
    [InitializableModule]
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class CmsIntegrationModule : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            //Add initialization logic, this method is called once after CMS has been initialized
            DataFactory.Instance.PublishingContent += Instance_PublishingBlock;
        }

        public void Preload(string[] parameters) { }

        public void Uninitialize(InitializationEngine context)
        {
            //Add uninitialization logic
            DataFactory.Instance.PublishingContent -= Instance_PublishingBlock;
        }

        private static void Instance_PublishingBlock(object sender, ContentEventArgs e) { }
    }
}