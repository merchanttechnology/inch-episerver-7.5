﻿using System.Data.Entity;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc;
using EPiServer.Web.Mvc.Html;
using Ineos.Business.Rendering;
using Ineos.Database.Model;
using Ineos.Database.Model.Tables;
using Ineos.Database.Model.Views;
using Ineos.Database.Repository;
using Ineos.Database.Service;
using StructureMap;

namespace Ineos.Business.Initialization
{
    [InitializableModule]
    public class DependencyResolverInitialization : IConfigurableModule
    {
        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            context.Container.Configure(ConfigureContainer);
            System.Web.Mvc.DependencyResolver.SetResolver(new StructureMapDependencyResolver(context.Container));
        }

        private static void ConfigureContainer(ConfigurationExpression container)
        {
            //Swap out the default ContentRenderer for our custom
            container.For<IContentRenderer>().Use<ErrorHandlingContentRenderer>();
            container.For<ContentAreaRenderer>().Use<IneosContentAreaRenderer>();

            container.For<IUnitOfWork>().Use<UnitOfWork>();
            container.For<DbContext>().Use<ProductsDBContext>();

            //Implementations for custom interfaces can be registered here.
            Bind<fieldsdefinition>(container);
            Bind<sqldefinition>(container);
            Bind<v_published_productdata>(container);
            Bind<Keyword>(container);
            Bind<KeywordReference>(container);
            Bind<productdata>(container);

            //Initialize businesses
            Bind<barex>(container);
            Bind<ab>(container);
            Bind<chlorvinyl>(container);
            Bind<enterprise>(container);
            Bind<nitrile>(container);
            Bind<oligomer>(container);
            Bind<opeu>(container);
            Bind<opu>(container);
            Bind<oxide>(container);
            Bind<paraform>(container);
            Bind<phenol>(container);
            Bind<refining>(container);
            Bind<styrenic>(container);
            Bind<Technology>(container);
        }

        public void Initialize(InitializationEngine context) { }

        public void Uninitialize(InitializationEngine context) { }

        public void Preload(string[] parameters) { }

        public static void Bind<T>(ConfigurationExpression container) where T : BaseEntity
        {
            container.For<IProductDataService<T>>().Use<ProductDataService<T>>();
            container.For<IProductRepository<T>>().Use<ProductDataRepository<T>>();
        }
    }
}