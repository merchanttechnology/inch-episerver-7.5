﻿namespace Ineos.Business.CustomTypes
{
    public enum ArticleSize
    {
        Small,
        Medium,
        Large
    }
}