﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Framework.Localization;
using EPiServer.Shell.ObjectEditing;

namespace Ineos.Business.CustomTypes
{
    public class EnumSelectionFactory<TEnum> : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var values = Enum.GetValues(typeof (TEnum));
            return (from object value in values select new SelectItem {Text = GetValueName(value), Value = value});
        }

        private string GetValueName(object value)
        {
            var staticName = Enum.GetName(typeof (TEnum), value);

            if (staticName != null)
            {
                string localizationPath = string.Format("/property/enum/{0}/{1}", typeof (TEnum).Name.ToLowerInvariant(),
                    staticName.ToLowerInvariant());

                string localizedName;

                return LocalizationService.Current.TryGetString(localizationPath, out localizedName) ? localizedName : staticName;
            }

            return string.Empty;
        }
    }
}