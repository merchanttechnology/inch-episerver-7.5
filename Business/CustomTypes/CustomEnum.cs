﻿using System;
using System.Web.Mvc;
using EPiServer.Shell.ObjectEditing;

namespace Ineos.Business.CustomTypes
{
    public class CustomEnum : SelectOneAttribute, IMetadataAware
    {
        public CustomEnum(Type enumType)
        {
            EnumType = enumType;
        }

        public Type EnumType { get; set; }

        public new void OnMetadataCreated(ModelMetadata metadata)
        {
            SelectionFactoryType = typeof (EnumSelectionFactory<>).MakeGenericType(EnumType);
            base.OnMetadataCreated(metadata);
        }
    }
}