﻿using System.ComponentModel;

namespace Ineos.Business.CustomTypes
{
    public enum HeaderBlockType
    {
        Product,
        Sustainability,
        [Description("Get In Touch")]
        GetInTouch,
        [Description("Quick Find")]
        QuickFind,
        News,
        General
    }

    public enum CarouselType
    {
        Site,
        Home
    }

    public enum CarouselWarningType
    {
        Error,
        Warning,
        Information,
        Off
    }
}